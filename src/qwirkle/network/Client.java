package qwirkle.network;

import qwirkle.controller.NetworkController;
import qwirkle.model.NetworkPlayer;
import qwirkle.model.Tile;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * The Class Client provides client functionality for a network game.
 */
public class Client implements Runnable {

  /** The name of the client. */
  private String name;
  
  /** The socket to connect to. */
  private Socket sock;
  
  /** The inputstream. */
  private BufferedReader in;
  
  /** The outputstream. */
  private BufferedWriter out;
  
  /** Whether the client is active or not. */
  private boolean active;

  /** The NetworkController that controls the game and this client. */
  private NetworkController game;
  
  /** Whether this client is a computer player. */
  private boolean isComputerPlayer;
  
  /** The thinking time of this (possible) computer player. */
  private int thinkingTime;

  //--- Supported features ---//
  /** Chat support. */
  protected String chat = "0";
  
  /** Challenge support. */
  protected String challenge = "0";
  
  /** Leaderboard support. */
  protected String leaderboard = "0";
  
  /** Security support. */
  protected String security = "0";

  /**
   * Create client object, try to connect to host and set up reader/writer.
   * @param name Name of the new client
   * @param host InetAddress of the server to connect to
   * @param port Port number of the server to connect to
   * @param isComputerPlayer Whether or not this client is computer controlled
   * @param thinkingTime Thinking time of computer player in milliseconds
   */
  public Client(String name, InetAddress host, int port, boolean isComputerPlayer, 
      int thinkingTime) {
    this.name = name;
    this.isComputerPlayer = isComputerPlayer;
    this.thinkingTime = thinkingTime;
    try {
      sock = new Socket(host, port);
      in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
      out = new BufferedWriter(new OutputStreamWriter(sock.getOutputStream()));
    } catch (IOException e) {
      System.out.println("Er ging iets mis met het aanmaken van een socket. "
          + "Wellicht heb je een verkeerde server of poort ingevoerd. Probeer het opnieuw.");
      main(new String[0]);
    }
    this.run();
  }
  
  /**
   * Initialize a Client for testing without a server.
   * @param name Name of client
   * @param isComputerPlayer Whether or not this client is computer controlled
   * @param thinkingTime Thinking time of computer player in milliseconds
   */
  public Client(String name, boolean isComputerPlayer, int thinkingTime) {
    this.name = name;
    this.isComputerPlayer = isComputerPlayer;
    this.thinkingTime = thinkingTime;
  }

  /**
   * Set client state to active, send joinRequest to server and start receiving
   * messages.
   */
  public void run() {
    active = true;
    sendJoinRequest();
    receiveMessage();
  }

  /**
   * Sends a joinRequest to the server.
   */
  public void sendJoinRequest() {
    sendMsg(Protocol.Client.JOINREQUEST + " " + getName() + " " + getSupportedFeatures());
  }

  /**
   * Read a message from the server and send the message to the incomingCommand,
   * which processes it.
   */
  public void receiveMessage() {
    String message;
    try {
      while (active && (message = in.readLine()) != null) {
        incomingCommand(message);
      }
      
    } catch (IOException e) {
      System.out.println("Er is iets mis met de verbinding met de server. "
          + "De verbinding wordt verbroken.");
      shutdown();
    }
    
    System.out.println("Qwirkle wordt afgesloten...");
    shutdown();
  }

  /**
   * Process an incoming command.
   * @param command String with incoming command from server, to be processed
   */
  public void incomingCommand(String command) {
    String[] parts = command.split(" ");
    switch (parts[0]) {
      case Protocol.Server.ACCEPTREQUEST:
        System.out.println("Verbinding met server geslaagd!");
        if (parts[1].equals(getName())) {
          sendGameRequest();
        }
        break;
      case Protocol.Server.STARTGAME:
        System.out.println("Het spel wordt gestart met de volgende spelers:");
        game = new NetworkController(this);
        int error = 0;
        for (int i = 1; i < (parts.length); i++) {
          if (parts[i].matches("[a-zA-Z]+")) {
            System.out.println("- " + parts[i]);
            NetworkPlayer newPlayer = new NetworkPlayer(parts[i], game, isComputerPlayer, 
                thinkingTime);
            game.addPlayer(newPlayer);
          } else {
            error = 1;
          }
        }
        if (error == 0) {
          game.start();
        }
        break;
      case Protocol.Server.CONNECTIONLOST:
        System.out.println("De server heeft de verbinding met " + parts[1] 
            + " verloren. Het spel is gestopt.");
        if (getYesOrNo("Wilt u opnieuw spelen?")) {
          sendGameRequest();
        } else {
          active = false;
        }
        break;
      case Protocol.Server.GAMEOVER:
        System.out.println("This game is over. These are the scores:");
        game.showScores();
        if (getYesOrNo("Wilt u opnieuw spelen?")) {
          sendGameRequest();
        } else {
          active = false;
        }
        break;
      case Protocol.Server.GIVESTONES:
        int amountOfTiles = 0;
        try {
          amountOfTiles = Integer.parseInt(parts[1]);
        } catch (NumberFormatException e) {
          System.out.println("Er ging iets mis in het verwerken je nieuwe stenen.");
        }
  
        System.out.println("Je hebt " + amountOfTiles + " nieuwe stenen gekregen:");
        List<Tile> tiles = new ArrayList<Tile>();
        for (int i = 0; i < amountOfTiles; i++) {
          int start = 2 + (i * 3);
          Tile tile = new Tile(Protocol.getType(parts[start]), Protocol.getColor(parts[start + 1]));
          tiles.add(tile);
          System.out.println("- " + tile.toWords());
        }
        for (Tile t : tiles) {
          game.addTileToPlayer(t, getName());
        }
        break;
      case Protocol.Server.INVALIDCOMMAND:
        String reason = "none. ";
        if (parts.length >= 2) {
          reason = "";
          for (int i = 1; i<parts.length; i++) {
            reason += parts[i] + " ";
          }
        }
        System.out.println("Invalid command received from server. Reason: " + reason + "Client shutting down.");
        break;
      case Protocol.Server.MOVEREQUEST:
        game.showBoardAndInventory();
        game.determineMove(getName());
        break;
      case Protocol.Server.NOTIFYMOVE:
        // Example Code: "notifymove Ruud 3 2 4 5 3 4 | 4 1 3 5"
        if (!parts[1].equals(getName())) {
          // Is other player
          int amountOfStones = 0;
          try {
            amountOfStones = Integer.parseInt(parts[3]);
          } catch (NumberFormatException e) {
            System.out.println("Er ging iets mis in het verwerken van een "
                + "zet van een andere speler.");
          }
  
          System.out.println("Speler " + parts[1] + " heeft de volgende "
                + amountOfStones + " stenen geplaatst:");
  
          for (int i = 0; i < amountOfStones; i++) {
            int start = 4 + (i * 5);
            Tile tile = new Tile(Protocol.getType(parts[start]), 
                Protocol.getColor(parts[start + 1]));
            String coords = parts[start + 2] + " " + parts[start + 3];
            game.setClientMove(coords, tile, parts[1]);
            System.out.println("- " + tile.toWords() + " op " + coords);
          }
          game.setClientScore(parts[1]);
        }
        break;
      case Protocol.Server.NOTIFYTRADE:
        if (!parts[1].equals(getName())) {
          // Is other player
          int amountOfStones = 0;
          try {
            amountOfStones = Integer.parseInt(parts[2]);
          } catch (NumberFormatException e) {
            System.out.println("Er ging iets mis in het verwerken van een "
                + "ruil van een andere speler.");
          }
          System.out.println(parts[1] + " heeft " + amountOfStones + " stenen geruild");
        }
        break;
      default:
        // Invalid command, negeren.
    }
  }

  /**
   * Gets the name of this client.
   * @return the name of this client
   */
  public String getName() {
    return name;
  }

  /**
   * Sends a gameRequest to the server by asking for the amount of players.
   */
  public void sendGameRequest() {
    Scanner input = new Scanner(System.in);

    int amount = 0;
    String init = null;
    System.out.println("U kunt nu een game starten. Met hoeveel spelers? (2-4)");
    while (init == null || amount == 0) {
      init = input.hasNextLine() ? input.nextLine() : null;
      if (init != null) {
        int aa;
        try {
          aa = Integer.parseInt(init);
        } catch (NumberFormatException e) {
          aa = 0;
        }
        if (2 <= aa && aa <= 4) {
          amount = aa;
        } else {
          System.out.println("Error: geef een getal tussen 2 en 4 op!");
        }
      }
    }
    sendMsg(Protocol.Client.GAMEREQUEST + " " + amount);
    System.out.println("De gamerequest voor " + amount 
        + " spelers is verstuurd. Het spel begint zodra er genoeg spelers zijn.");
  }

  /**
   * Sends a full move to the server.
   * @param moves the moves the client placed during its turn.
   */
  public void sendSetMove(String moves) {
    sendMsg(Protocol.Client.SETMOVE + " " + moves);
  }
  
  /**
   * Sends a trade to the server.
   * @param tiles the tiles to trade.
   */
  public void sendTrade(String tiles) {
    sendMsg(Protocol.Client.DOTRADE + " " + tiles);
  }

  /**
   * Send a command to the server clienthandler.
   * @param msg the command
   */
  private void sendMsg(String msg) {
    try {
      out.write(msg);
      out.newLine();
      out.flush();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * Flushes input- and outputstreams and closes socket.
   */
  public void shutdown() {
    System.out.println("Closing socket connection...");
    try {
      in.close();
      out.flush();
      out.close();
      sock.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
    
  }

  /**
   * Gets the supported features.
   * @return the supported features
   */
  public String getSupportedFeatures() {
    return chat + " " + challenge + " " + leaderboard + " " + security;
  }
  
  /**
   * Ask user a yes/no question and convert to a boolean.
   * @param question Question to ask to user, to be answered with yes or no.
   * @return True if user answered Yes, False if user answered No
   */
  public static boolean getYesOrNo(String question) {
    Scanner in = new Scanner(System.in);
    boolean result = false;
    
    String input = null;
    boolean init = false;
    System.out.println(question);
    while (input == null || !init) {
      input = in.hasNextLine() ? in.nextLine() : null;
      if (input != null) {
        if (input.equals("Y")) {
          result = true;
          init = true;
        } else if (input.equals("N")) {
          init = true;
        } else {
          System.out.println("Voer 'Y' of 'N' in. " + question);
        }
      }
    }
    
    return result;
  }

  /**
   * Starts a client. Asks for name, internet address, port, and type of player
   * @param args Not needed
   */
  public static void main(String[] args) {

    Scanner in = new Scanner(System.in);

    String name = "";
    

    String inputName = null;
    System.out.println("Geef een naam op voor deze client");
    while (inputName == null || name.equals("")) {
      inputName = in.hasNextLine() ? in.nextLine() : null;
      if (inputName != null) {
        if (inputName.matches("[a-zA-Z]+")) {
          name = inputName;
        } else {
          System.out.println("Geef een geldige naam op (alleen letters)!");
        }
      }
    }

    boolean iscomputer = getYesOrNo("Is " + name + " een computerspeler? (Y/N)");
    
    int thinkingTime = 0;
    if (iscomputer) {
      String inputTime = null;
      System.out.println("Geef de denktijd van de computerspeler op in milliseconden.");
      while (inputTime == null || thinkingTime == 0) {
        inputTime = in.hasNextLine() ? in.nextLine() : null;
        if (inputTime != null) {
          int aa;
          try {
            aa = Integer.parseInt(inputTime);
          } catch (NumberFormatException e) {
            aa = 0;
          }
          if (aa > 0) {
            thinkingTime = aa;
          } else {
            System.out.println("Geef een geldige denktijd (> 0) op!");
          }
        }
      }
    }

    InetAddress host = null;
    String inputHost = "";
    System.out.println("Geef een internetadres op");
    while (host == null || inputHost.equals("")) {
      inputHost = in.hasNextLine() ? in.nextLine() : null;
      if (inputHost != null) {
        try {
          host = InetAddress.getByName(inputHost);
        } catch (UnknownHostException e) {
          System.out.println("Ongeldig internetadres. Geef een geldig adres op");
        }
      }
    }

    int port = 0;
    String inputPort = null;
    System.out.println("Geef een poort op");
    while (inputPort == null || port == 0) {
      inputPort = in.hasNextLine() ? in.nextLine() : null;
      if (inputPort != null) {
        int aa;
        try {
          aa = Integer.parseInt(inputPort);
        } catch (NumberFormatException e) {
          aa = 0;
        }
        if (1024 <= aa && aa <= 65535) {
          port = aa;
        } else {
          System.out.println("Vul een geldig poortnummer (1024 - 65535) in!");
        }
      }
    }

    new Client(name, host, port, iscomputer, thinkingTime);
    
  }

}
