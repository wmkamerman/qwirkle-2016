package qwirkle.network;

import qwirkle.controller.NetworkController;
import qwirkle.model.Player;
import qwirkle.model.Tile;

import java.io.IOException;
import java.io.PrintStream;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * The Class Server provides all server functionality.
 */
public class Server implements Runnable {

  /** The System.out printstream. */
  private PrintStream out = new PrintStream(System.out);
  
  /** The error printstream. */
  private PrintStream err = new PrintStream(System.err);

  /** The server socket. */
  private ServerSocket serverSocket;
  
  /** The connected clients. */
  private List<ClientHandler> connectedClients;
  
  /** The clients in game. */
  private List<ClientHandler> clientsInGame;
  
  /** The clients in a 2-player game. */
  List<ClientHandler> c2;
  
  /** The clients in a 3-player game. */
  List<ClientHandler> c3;

  /** Whether or not the server is active. */
  private boolean active;
  
  /** Chat support. */
  private String chat = "0";
  
  /** Challenge support. */
  private String challenge = "0";
  
  /** Leaderboard support. */
  private String leaderboard = "0";
  
  /** Security support. */
  private String security = "0";
  
  /** Scanner for TUI input. */
  private static Scanner in;
  
  /**
   * Constructs a server instance by initializing the list of connected clients.
   * @param port Portnumber to start listening on.
   */
  public Server(int port) {
    try {
      serverSocket = new ServerSocket(port);
    } catch (BindException e) {
      err.println("Deze poort is al in gebruik. Gebruik een andere poort.");
      Server srv = new Server(getPort());
      srv.run();
    } catch (IOException e) {
      err.println("Er ging iets mis met het aanmaken van de ServerSocket. Het volgende ging mis:");
      e.printStackTrace();
    }
    out.println("Server has started listening on port " + port);
    
    active = true;
    connectedClients = new ArrayList<ClientHandler>();
    clientsInGame = new ArrayList<ClientHandler>();
    c2 = new ArrayList<ClientHandler>();
    c3 = new ArrayList<ClientHandler>();
  }
  
  /**
   * Initialize the server by asking for port, creating Server instance and running it.
   * @param args Not needed.
   */
  public static void main(String[] args) {
    Server srv = new Server(getPort());
    srv.run();
  }
  
  /**
   * Get portnumber to listen on.
   * @return Portnumber (1024 - 65535) as integer
   */
  public static int getPort() {
    in = new Scanner(System.in);
    int port = 0;
    String inputPort = null;
    System.out.println("Geef een poort op");
    while (inputPort == null || port == 0) {
      inputPort = in.hasNextLine() ? in.nextLine() : null;
      if (inputPort != null) {
        int aa;
        try {
          aa = Integer.parseInt(inputPort);
        } catch (NumberFormatException e) {
          aa = 0;
        }
        if (1024 <= aa && aa <= 65535) {
          port = aa;
        } else {
          System.out.println("Vul een geldig poortnummer (1024 - 65535) in!");
        }
      }
    }
    return port;
  }

  /**
   * Start the server: accept incoming connections and assign a clienthandler for each connection. 
   */
  @Override
  public void run() {
    try {
      while (active) {
        Socket sock = serverSocket.accept();
        ClientHandler handler = new ClientHandler(this, sock);
        (new Thread(handler)).start();
        addHandler(handler);
        out.println("Er is een client verbonden!");
      }
    } catch (IOException e) {
      out.println("Kan niet met client verbinden. Het volgende ging mis:");
      e.printStackTrace();
    }
  }

  /**
   * Add a ClientHandler to the collection of ClientHandlers.
   * @param handler ClientHandler that will be added
   */
  public void addHandler(ClientHandler handler) {
    connectedClients.add(handler);
  }

  /**
   * Remove a ClientHandler from the collection of ClientHandlers.
   * @param handler ClientHandler that will be removed
   */
  public void removeHandler(ClientHandler handler) {
    handler.shutdown();
    connectedClients.remove(handler);
  }

  /**
   * Add a ClientHander to the collection of ClientHanders in game.
   * @param handler ClientHander to be added
   */
  public void addToClientsInGame(ClientHandler handler) {
    clientsInGame.add(handler);
  }

  /**
   * Remove a ClientHander from the collection of ClientHanders in game.
   * @param handler ClientHander to be removed
   */
  public void removeFromClientsInGame(ClientHandler handler) {
    clientsInGame.remove(handler);
  }

  /**
   * Process an incoming command from a client.
   * @param client ClientHandler receiving the command
   * @param command Command to be processed.
   */
  public void incomingCommand(ClientHandler client, String command) {
    System.out.println("Incoming Command from " + client.getName() + ": " + command);
    String[] parts = command.split(" ");
    switch (parts[0]) {
      case Protocol.Client.JOINREQUEST:
        client.setName(parts[1]);
        sendAcceptRequest(client);
        break;
      case Protocol.Client.GAMEREQUEST:
        try {
          client.setAmountOfPlayers(Integer.parseInt(parts[1]));
          startGame();
        } catch (NumberFormatException e) {
          removeHandler(client);
        }
        break;
      case Protocol.Client.SETMOVE:
        for (int i = 0; i < Integer.parseInt(parts[1]); i++) {
          int start = 2 + (5 * i);
  
          String coords = parts[start + 2] + " " + parts[start + 3];
          Tile tile = new Tile(Protocol.getType(parts[start]), Protocol.getColor(parts[start + 1]));
  
          client.getController().setMove(coords, tile, client.getPlayer(), true);
        }
  
        int score = client.getController().calculateScore(client.getPlayer(), true);
        client.getPlayer().addToScore(score);
  
        client.getPlayer().clearNewTiles();
  
        String notify = client.getName() + " " + score + " ";
        for (int i = 1; i < parts.length; i++) {
          notify += parts[i] + " ";
        }
        sendNotifyMove(notify, client);
  
        client.getController().setClientHasSetMove();
        break;
      case Protocol.Client.DOTRADE:
        
        for (int i = 0; i < Integer.parseInt(parts[1]); i++) {
          int start = 2 + (3 * i);
          Tile tile = new Tile(Protocol.getType(parts[start]), Protocol.getColor(parts[start + 1]));
  
          client.getPlayer().removeTile(tile);
        }
        
        sendNotifyTrade(client.getName() + " " + parts[1], client);
        client.getController().setClientHasSetMove();
        break;
      default:
        client.sendMessage(Protocol.Server.INVALIDCOMMAND);
        client.shutdown(); 
    }
  }

  /**
   * Starts a game.
   */
  public void startGame() {
    for (ClientHandler client : connectedClients) {
      if (!clientsInGame.contains(client)) {
        // Client not in-game
        if (client.getAmountOfPlayers() == 2) {
          if (c2.size() < 2) {
            c2.add(client);
            if (c2.size() == 2) {
              sendStartGame(c2);
            }
          }
        } else if (client.getAmountOfPlayers() == 3) {
          if (c3.size() < 3) {
            c3.add(client);
            if (c3.size() == 3) {
              sendStartGame(c3);
            }
          }
        } else if (client.getAmountOfPlayers() == -1) {
          // do nothing
        } else {
          client.sendMessage(Protocol.Server.INVALIDCOMMAND + " alleen 2 en 3 spelers worden ondersteund.");
          client.shutdown();
        }
      }
    }
    
    if (c2.size() != 2) {
      c2.clear();
    }
    
    if (c3.size() != 3) {
      c3.clear();
    }
    
  }

  /**
   * Notifies all clients of a move, according to the protocol.
   * @param move Command to send to all players, excluding the notifymove protocol command.
   * @param clientHandler ClientHandler of client that set move.
   */
  public void sendNotifyMove(String move, ClientHandler clientHandler) {
    String message = Protocol.Server.NOTIFYMOVE + " " + move;
    if (c2.contains(clientHandler)) {
      for (ClientHandler client : c2) {
        client.sendMessage(message);
      }
    } else if (c3.contains(clientHandler)) {
      for (ClientHandler client : c3) {
        client.sendMessage(message);
      }
    }
  }
  
  /**
   * Notifies all clients of a trade, according to the protocol.
   * @param command Command to send to all players, excluding the protocol command.
   * @param clientHandler ClientHandler of client that traded tiles.
   */
  public void sendNotifyTrade(String command, ClientHandler clientHandler) {
    String message = Protocol.Server.NOTIFYTRADE + " " + command;
    if (c2.contains(clientHandler)) {
      for (ClientHandler client : c2) {
        client.sendMessage(message);
      }
    } else if (c3.contains(clientHandler)) {
      for (ClientHandler client : c3) {
        client.sendMessage(message);
      }
    }
  }

  /**
   * Starts a game with a list of clients. 
   * Sends all clients a startgame message and starts the game with those players.
   * @param clients List of clients to start a game with.
   */
  public void sendStartGame(List<ClientHandler> clients) {

    NetworkController game = new NetworkController(this);

    String command = Protocol.Server.STARTGAME;
    // Build startgame command to clients
    for (ClientHandler client : clients) {
      command += " " + client.getName();
    }

    for (ClientHandler client : clients) {
      client.sendMessage(command);
      addToClientsInGame(client);
      client.setPlayer(game);
      game.addPlayer(client.getPlayer());
    }

    game.start();
  }

  /**
   * Send a move request to a client.
   * @param client the clienthandler to send a moverequest to.
   */
  public void sendMoveRequest(ClientHandler client) {
    client.sendMessage(Protocol.Server.MOVEREQUEST);
  }
  
  /**
   * Send Givestones command to a client.
   * @param name Name of client
   * @param tiles Tiles to give to client as a string according to protocol. 
   * @param tilesToGive Tiles to give to client as a list (to fill up inventory of CommandPlayer)
   */
  public void sendGiveStones(String name, String tiles, List<Tile> tilesToGive) {
    for (ClientHandler client : connectedClients) {
      if (client.getName().equals(name)) {

        // Fill inventory of CommandPlayer!
        client.getPlayer().fillInventory(tilesToGive);

        client.sendMessage(Protocol.Server.GIVESTONES + " " + tiles);
      }
    }
  }

  /**
   * Send Gameover message to all clients.
   * @param clientHandler the clienthandler of one of the clients.
   */
  public void sendGameOver(ClientHandler clientHandler) {
    // Code: "gameover Wessel 67 | Teun 45 | Rik 54"
    String result = Protocol.Server.GAMEOVER + " ";
    
    List<ClientHandler> listOfClients = new ArrayList<ClientHandler>();
    
    if (c2.contains(clientHandler)) {
      listOfClients = c2;
    } else if (c3.contains(clientHandler)) {
      listOfClients = c3;
    }

    for (int i = 0; i < listOfClients.size(); i++) {
      Player player = connectedClients.get(i).getPlayer();
      result += player.getName() + " " + player.getScore();
      if ((i + 1) != listOfClients.size()) {
        // Not the last one
        result += " | ";
      }
    }

    for (ClientHandler client : listOfClients) {
      client.sendMessage(result);
    }

  }

  /**
   * Gets the supported features.
   * @return the supported features
   */
  public String getSupportedFeatures() {
    return chat + " " + challenge + " " + leaderboard + " " + security;
  }

  /**
   * Send acceptrequest to a client.
   * @param client the client to accept.
   */
  public void sendAcceptRequest(ClientHandler client) {
    client.sendMessage(Protocol.Server.ACCEPTREQUEST + " " + client.getName() + " " 
        + getSupportedFeatures());
  }

}
