/**
 * Provides client/server functionality and the protocol implementation.
 */
package qwirkle.network;