package qwirkle.network;

import qwirkle.controller.NetworkController;
import qwirkle.model.CommandPlayer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

/**
 * The Class ClientHandler provides server functionality to connect to a client.  
 */
public class ClientHandler implements Runnable {

  /** The inputstream. */
  private BufferedReader in;
  
  /** The outputstream. */
  private BufferedWriter out;

  /** The server that controls this clienthandler. */
  private Server server;
  
  /** The socket. */
  private Socket sock;
  
  /** Whether this clienthandler is active of not. */
  private boolean active;

  /** The name of this clienthandler. */
  private String name;
  
  /** The amount of players. */
  private int amountOfPlayers;
  
  /** The commandplayer assigned to this clienthandler. */
  private CommandPlayer player;
  
  /** The networkcontroller that controls the game. */
  private NetworkController controller;

  /**
   * Constructs a ClientHandler with a server and a socket.
   * @param srv Server object the ClientHandler is connected to
   * @param sock Socket the ClientHandler must use
   */
  public ClientHandler(Server srv, Socket sock) {
    this.server = srv;
    this.sock = sock;
    try {
      in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
      out = new BufferedWriter(new OutputStreamWriter(sock.getOutputStream()));
    } catch (IOException e) {
      e.printStackTrace();
    }
    this.amountOfPlayers = -1;
  }

  /**
   * Start the clienthandler process.
   */
  @Override
  public void run() {
    active = true;
    while (active) {
      receiveMessages();
    }
  }

  /**
   * Receive messages from client and send them to the server.
   */
  private void receiveMessages() {
    try {
      String incoming = in.readLine();
      if (incoming != null) {
        server.incomingCommand(this, incoming);
      }
    } catch (IOException e) {
      System.out.println("Error: could not read message. Assuming client disconnected.");
      shutdown();
    }
  }

  /**
   * Send message from ClientHandler to Client.
   * @param message Message to send
   */
  public void sendMessage(String message) {
    try {
      out.write(message);
      System.out.println("Outgoing message to " + name + ": " + message);
      out.newLine();
      out.flush();
    } catch (IOException e) {
      System.out.println("Error: could not read message. Assuming client disconnected.");
      shutdown();
    }
  }

  /**
   * Shutdown the ClientHandler by closing the input- and outputstream and the socket.  
   */
  public void shutdown() {
    active = false;
    System.out.println("Clienthandler " + getName() + " shutting down.");
    try {
      in.close();
      out.flush();
      out.close();
      sock.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * Sets the name of this clienthandler. 
   * @param name the name to set
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Gets the name of this clienthandler.
   * @return the name of this clienthandler
   */
  public String getName() {
    return this.name;
  }

  /**
   * Sets the amount of players.
   * @param amount the new amount of players
   */
  public void setAmountOfPlayers(int amount) {
    this.amountOfPlayers = amount;
  }

  /**
   * Gets the amount of players.
   * @return the amount of players
   */
  public int getAmountOfPlayers() {
    return this.amountOfPlayers;
  }

  /**
   * Creates a player and assigns it to this clienthandler.
   * @param controller the networkcontroller that controls this game.
   */
  public void setPlayer(NetworkController controller) {
    player = new CommandPlayer(name, controller, this);
    this.controller = controller;
  }

  /**
   * Gets the networkcontroller assigned to this clienthandler.
   * @return the networkcontroller assigned to this clienthandler
   */
  public NetworkController getController() {
    return controller;
  }

  /**
   * Gets the commandplayer of this clienthandler.
   * @return the commandplayer
   */
  public CommandPlayer getPlayer() {
    return player;
  }
}
