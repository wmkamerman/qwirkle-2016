package qwirkle.network;

import qwirkle.model.Tile.Color;
import qwirkle.model.Tile.Type;

/**
 * The Class Protocol implements the protocol and provides functions to help implementing 
 * the protocol.
 */
public class Protocol {

  /**
   * Server to client messages.
   * @author wimkamerman
   */
  public class Server {
    
    /** The Constant ACCEPTREQUEST. */
    public static final String ACCEPTREQUEST = "acceptrequest";
    
    /** The Constant STARTGAME. */
    public static final String STARTGAME = "startgame";
    
    /** The Constant GIVESTONES. */
    public static final String GIVESTONES = "givestones";
    
    /** The Constant MOVEREQUEST. */
    public static final String MOVEREQUEST = "moverequest";
    
    /** The Constant NOTIFYMOVE. */
    public static final String NOTIFYMOVE = "notifymove";
    
    /** The Constant NOTIFYTRADE. */
    public static final String NOTIFYTRADE = "notifytrade";
    
    /** The Constant GAMEOVER. */
    public static final String GAMEOVER = "gameover";
    
    /** The Constant INVALIDCOMMAND. */
    public static final String INVALIDCOMMAND = "invalidcommand";
    
    /** The Constant CONNECTIONLOST. */
    public static final String CONNECTIONLOST = "connectionlost";
  }

  /**
   * Client to server messages.
   * @author wimkamerman
   */
  public static class Client {
    
    /** The Constant JOINREQUEST. */
    public static final String JOINREQUEST = "joinrequest";
    
    /** The Constant GAMEREQUEST. */
    public static final String GAMEREQUEST = "gamerequest";
    
    /** The Constant SETMOVE. */
    public static final String SETMOVE = "setmove";
    
    /** The Constant DOTRADE. */
    public static final String DOTRADE = "givestones";
  }

  /**
   * Function to get Type object by number defined in protocol.
   * @param number String with only one integer, 1-6
   * @return Type object corresponding to number entered, defined by protocol. Null if invalid.
   */
  public static Type getType(String number) {
    Type type = null;
    if (number.equals("1")) {
      type = Type.CIRCLE;
    } else if (number.equals("2")) {
      type = Type.CROSS;
    } else if (number.equals("3")) {
      type = Type.DIAMOND;
    } else if (number.equals("4")) {
      type = Type.RECTANGLE;
    } else if (number.equals("5")) {
      type = Type.STAR;
    } else if (number.equals("6")) {
      type = Type.CLUBS;
    }
    return type;
  }

  /**
   * Function to get Color object by number defined in protocol.
   * @param number String with only one integer, 1-6
   * @return Color object corresponding to number entered, defined by protocol. Null if invalid. 
   */
  public static Color getColor(String number) {
    Color color = null;
    if (number.equals("1")) {
      color = Color.RED;
    } else if (number.equals("2")) {
      color = Color.ORANGE;
    } else if (number.equals("3")) {
      color = Color.YELLOW;
    } else if (number.equals("4")) {
      color = Color.GREEN;
    } else if (number.equals("5")) {
      color = Color.BLUE;
    } else if (number.equals("6")) {
      color = Color.PURPLE;
    }
    return color;
  }
}
