package qwirkle.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import qwirkle.model.Tile.Color;
import qwirkle.model.Tile.Type;
import qwirkle.network.Protocol;

/**
 * The Class ProtocolTest.
 */
public class ProtocolTest {

  /**
   * Test the getType function.
   */
  @Test
  public void testGetType() {
    assertEquals(Type.CIRCLE, Protocol.getType("1"));
    assertEquals(Type.CROSS, Protocol.getType("2"));
    assertEquals(Type.DIAMOND, Protocol.getType("3"));
    assertEquals(Type.RECTANGLE, Protocol.getType("4"));
    assertEquals(Type.STAR, Protocol.getType("5"));
    assertEquals(Type.CLUBS, Protocol.getType("6"));
    
    assertNull(Protocol.getType("a"));
  }

  /**
   * Test the getColor function.
   */
  @Test
  public void testGetColor() {
    assertEquals(Color.RED, Protocol.getColor("1"));
    assertEquals(Color.ORANGE, Protocol.getColor("2"));
    assertEquals(Color.YELLOW, Protocol.getColor("3"));
    assertEquals(Color.GREEN, Protocol.getColor("4"));
    assertEquals(Color.BLUE, Protocol.getColor("5"));
    assertEquals(Color.PURPLE, Protocol.getColor("6"));
    
    assertNull(Protocol.getType("a"));
  }
  
}
