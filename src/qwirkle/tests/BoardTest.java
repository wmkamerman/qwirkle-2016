package qwirkle.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import qwirkle.model.Board;
import qwirkle.model.Tile;
import qwirkle.model.Tile.Color;
import qwirkle.model.Tile.Type;

import java.util.LinkedHashMap;

/**
 * The Class BoardTest tests the Board class.
 */
public class BoardTest {
  
  /** The board. */
  private Board board;
  
  /**
   * Instantiate the board.
   */
  @Before
  public void setUp() {
    board = new Board();
  }

  /**
   * Check if board is empty on initialization.
   */
  @Test
  public void testInitial() {
    assertEquals(0, board.getTilesOnBoard().size());
  }
  
  /**
   * Test the setMove function by creating two tiles and placing them on the board.
   * Then check if the size of the tiles on board hashmap is 2.
   * Then place another tile at the place of the second tile. This one should not be placed.
   * Getting the second tile should equal the original second tile.
   */
  @Test
  public void testSetMove() {
    Tile t1 = new Tile(Type.CROSS, Color.BLUE);
    Tile t2 = new Tile(Type.DIAMOND, Color.RED);
    board.setMove("0 0", t1, false);
    board.setMove("0 1", t2, true);
    assertEquals(2, board.getTilesOnBoard().size());
    board.setMove("0 1", new Tile(Type.CIRCLE, Color.GREEN), false);
    assertEquals(t2, board.getTilesOnBoard().get("0 1"));
  }
  
  /**
   * Test if the ToPrint function works properly.
   */
  @Test
  public void testToPrint() {
    assertEquals("", board.toPrint());
    
    Tile t1 = new Tile(Type.CROSS, Color.BLUE);
    Tile t2 = new Tile(Type.DIAMOND, Color.RED);
    board.setMove("0 0", t1, true);
    board.setMove("0 1", t2, true);
    
    assertEquals("", board.toPrint());
  }
  
  /**
   * Check if the GetHorizontalTiles function gets the correct tile amount.
   */
  @Test
  public void testGetHorizontalTiles() {
    //Coordinates non-integer
    assertNull(board.getHorizontalTiles("a 0"));
    
    Tile t1 = new Tile(Type.CROSS, Color.BLUE);
    Tile t2 = new Tile(Type.DIAMOND, Color.RED);
    Tile t3 = new Tile(Type.RECTANGLE, Color.YELLOW);
    board.setMove("0 0", t1, true);
    board.setMove("1 0", t2, true);
    board.setMove("-1 0", t3, true);
    board.setMove("-3 0", t3, true);
    
    //Not counting t3, because it is not connected to t2.
    assertEquals(2, board.getHorizontalTiles("0 0").size());
    
  }

  /**
   * Check if the GetVerticalTiles function gets the correct tile amount.
   */
  @Test
  public void testGetVerticalTiles() {
    //Coordinates non-integer
    assertNull(board.getVerticalTiles("a b"));
    
    Tile t1 = new Tile(Type.CROSS, Color.BLUE);
    Tile t2 = new Tile(Type.DIAMOND, Color.RED);
    Tile t3 = new Tile(Type.RECTANGLE, Color.YELLOW);
    board.setMove("0 0", t1, true);
    board.setMove("0 1", t2, true);
    board.setMove("0 -1", t3, true);
    board.setMove("0 -3", t3, true);
    
    //Not counting t3, because it is not connected to t2.
    assertEquals(2, board.getVerticalTiles("0 0").size());
    
  }
  
  /**
   * First test of the IsValidMove function: the basics.
   * On an empty board, a tile is placed at "0 0". This should be valid.
   * Now another tile is placed at "0 0". This should be invalid, 
   * because there is already a tile at that place.
   */
  @Test
  public void testIsValidMoveBasics() {
    LinkedHashMap<Tile, String> newTiles = new LinkedHashMap<Tile, String>();
    String coords = "0 0";
    Tile t1 = new Tile(Type.CROSS, Color.BLUE);
    boolean silent = true;
    
    // Normal valid move
    assertTrue(board.isValidMove(coords, t1, newTiles, silent));
    
    // Tile already at that place
    Tile t2 = new Tile(Type.DIAMOND, Color.YELLOW);
    board.setMove("0 0", t1, true);
    assertFalse(board.isValidMove(coords, t2, newTiles, silent));
    
  }
  
  /**
   * Second test of the IsValidMove function: the Row Lengths.
   * Rows and colums are allowed to have a maximum length of 6. 
   * This is tested by placing 5 tiles in a row, adding the 6th one should be valid.
   * The 7th one must not be valid.
   * The experiment is then repeated for a column, after clearing the board first.
   */
  @Test
  public void testIsValidMoveRowLength() {
    final LinkedHashMap<Tile, String> newTiles = new LinkedHashMap<Tile, String>();
    final Tile newTile = new Tile(Type.DIAMOND, Color.GREEN);
    final boolean silent = false;
    
    // Horizontal row too long
    board.setMove("0 0", new Tile(Type.DIAMOND, Color.YELLOW), true);
    board.setMove("1 0", new Tile(Type.CIRCLE, Color.YELLOW), true);
    board.setMove("2 0", new Tile(Type.CLUBS, Color.YELLOW), true);
    board.setMove("3 0", new Tile(Type.CROSS, Color.YELLOW), true);
    board.setMove("4 0", new Tile(Type.STAR, Color.YELLOW), true);
    
    assertTrue(board.isValidMove("5 0", new Tile(Type.RECTANGLE, Color.YELLOW), newTiles, silent));
    board.setMove("5 0", new Tile(Type.RECTANGLE, Color.YELLOW), silent);
    
    assertFalse(board.isValidMove("6 0", newTile, newTiles, silent));
    
    // Vertical row too long
    board.clear();
    board.setMove("0 0", new Tile(Type.DIAMOND, Color.YELLOW), true);
    board.setMove("0 1", new Tile(Type.CIRCLE, Color.YELLOW), true);
    board.setMove("0 2", new Tile(Type.CLUBS, Color.YELLOW), true);
    board.setMove("0 3", new Tile(Type.CROSS, Color.YELLOW), true);
    board.setMove("0 4", new Tile(Type.STAR, Color.YELLOW), true);
    
    assertTrue(board.isValidMove("0 5", new Tile(Type.RECTANGLE, Color.YELLOW), newTiles, silent));
    board.setMove("0 5", new Tile(Type.RECTANGLE, Color.YELLOW), silent);
    
    assertFalse(board.isValidMove("0 6", newTile, newTiles, silent));
    
  }
  
  /**
   * Third test of the IsValidMove function: a tile must be placed next to another tile.
   * First a tile is placed on a empty board, this should be allowed.
   * Then, a tile is placed at "0 0". Placing another tile at "0 6" should not be allowed.
   * Placing that tile at "0 1" must be allowed. 
   */
  @Test
  public void testIsValidMoveAdjacent() {
    LinkedHashMap<Tile, String> newTiles = new LinkedHashMap<Tile, String>();
    boolean silent = false;
    
    // Test with empty board and tile at location "0 0"
    Tile t1 = new Tile(Type.RECTANGLE, Color.YELLOW);
    assertTrue(board.isValidMove("0 0", t1, newTiles, silent));
    
    // Test with tile at "0 0" and placing new one at "0 6"
    board.setMove("0 0", t1, silent);
    Tile t2 = new Tile(Type.CIRCLE, Color.YELLOW);
    assertFalse(board.isValidMove("0 6", t2, newTiles, silent));
    
    // Test with tile at "0 0" and placing new one at "0 1"
    assertTrue(board.isValidMove("0 1", t2, newTiles, silent));
  }
  
  /**
   * Fourth test of the IsValidMove function: a tile can only be placed next to another 
   * placed tile in one turn. To test this, 2 tiles will be placed on the board at "0 0" and "0 1".
   * Next, a third one is added at "0 2" and also added to the newTiles HashMap.
   * Now a fourth tile may only be placed at "0 3", "-1 2" and "1 2". This will be tested.
   * Also, 2 other locations will be tested: "0 -1" and "-1 0". They must be invalid.
   */
  @Test
  public void testIsValidMoveNewTiles() {
    LinkedHashMap<Tile, String> newTiles = new LinkedHashMap<Tile, String>();
    boolean silent = false;
    
    // Place two initial tiles.
    board.setMove("0 0", new Tile(Type.DIAMOND, Color.YELLOW), true);
    board.setMove("0 1", new Tile(Type.CIRCLE, Color.YELLOW), true);
    
    // Add third tile
    Tile t3 = new Tile(Type.RECTANGLE, Color.YELLOW);
    assertTrue(board.isValidMove("0 2", t3, newTiles, silent));
    board.setMove("0 2", t3, silent);
    newTiles.put(t3, "0 2");
    
    // Add the fourth tile
    Tile t4 = new Tile(Type.CLUBS, Color.YELLOW);
    assertTrue(board.isValidMove("0 3", t4, newTiles, silent));
    assertTrue(board.isValidMove("-1 2", t4, newTiles, silent));
    assertTrue(board.isValidMove("1 2", t4, newTiles, silent));
    
    assertFalse(board.isValidMove("0 -1", t4, newTiles, silent));
    assertFalse(board.isValidMove("-1 0", t4, newTiles, silent));
  }
  
  /**
   * Fifth test of the isValidMove function: check horizontal and vertical scenario 1 and 2.
   * Same type and different color horizontally. And same type and different color.
   */
  @Test
  public void testIsValidMoveHorizontalAndVertical() {
    final LinkedHashMap<Tile, String> newTiles = new LinkedHashMap<Tile, String>();
    final boolean silent = false;
    
    // Place five initial tiles.
    board.setMove("0 0", new Tile(Type.DIAMOND, Color.YELLOW), true);
    board.setMove("0 1", new Tile(Type.DIAMOND, Color.GREEN), true);
    board.setMove("0 -1", new Tile(Type.DIAMOND, Color.BLUE), true);
    board.setMove("-1 0", new Tile(Type.DIAMOND, Color.PURPLE), true);
    board.setMove("1 0", new Tile(Type.DIAMOND, Color.ORANGE), true);
    
    // Now at "-2 0" and "2 0" only blue, green and red diamond tiles can be placed.
    assertTrue(board.isValidMove("-2 0", new Tile(Type.DIAMOND, Color.BLUE), newTiles, silent));
    assertTrue(board.isValidMove("-2 0", new Tile(Type.DIAMOND, Color.GREEN), newTiles, silent));
    assertTrue(board.isValidMove("-2 0", new Tile(Type.DIAMOND, Color.RED), newTiles, silent));
    assertFalse(board.isValidMove("-2 0", new Tile(Type.CIRCLE, Color.BLUE), newTiles, silent));
    assertFalse(board.isValidMove("-2 0", new Tile(Type.CROSS, Color.GREEN), newTiles, silent));
    assertFalse(board.isValidMove("-2 0", new Tile(Type.STAR, Color.RED), newTiles, silent));
    assertFalse(board.isValidMove("-2 0", new Tile(Type.DIAMOND, Color.YELLOW), newTiles, silent));
    assertFalse(board.isValidMove("-2 0", new Tile(Type.DIAMOND, Color.PURPLE), newTiles, silent));
    assertFalse(board.isValidMove("-2 0", new Tile(Type.DIAMOND, Color.ORANGE), newTiles, silent));
    
    assertTrue(board.isValidMove("2 0", new Tile(Type.DIAMOND, Color.BLUE), newTiles, silent));
    assertTrue(board.isValidMove("2 0", new Tile(Type.DIAMOND, Color.GREEN), newTiles, silent));
    assertTrue(board.isValidMove("2 0", new Tile(Type.DIAMOND, Color.RED), newTiles, silent));
    assertFalse(board.isValidMove("2 0", new Tile(Type.RECTANGLE, Color.BLUE), newTiles, silent));
    assertFalse(board.isValidMove("2 0", new Tile(Type.CLUBS, Color.GREEN), newTiles, silent));
    assertFalse(board.isValidMove("2 0", new Tile(Type.STAR, Color.RED), newTiles, silent));
    assertFalse(board.isValidMove("2 0", new Tile(Type.DIAMOND, Color.YELLOW), newTiles, silent));
    assertFalse(board.isValidMove("2 0", new Tile(Type.DIAMOND, Color.PURPLE), newTiles, silent));
    assertFalse(board.isValidMove("2 0", new Tile(Type.DIAMOND, Color.ORANGE), newTiles, silent));
    
    // Now at "0 2" and "0 -2" only orange, purple and red diamond tiles can be placed.
    assertTrue(board.isValidMove("0 2", new Tile(Type.DIAMOND, Color.ORANGE), newTiles, silent));
    assertTrue(board.isValidMove("0 2", new Tile(Type.DIAMOND, Color.PURPLE), newTiles, silent));
    assertTrue(board.isValidMove("0 2", new Tile(Type.DIAMOND, Color.RED), newTiles, silent));
    assertFalse(board.isValidMove("0 2", new Tile(Type.CIRCLE, Color.ORANGE), newTiles, silent));
    assertFalse(board.isValidMove("0 2", new Tile(Type.CROSS, Color.PURPLE), newTiles, silent));
    assertFalse(board.isValidMove("0 2", new Tile(Type.STAR, Color.RED), newTiles, silent));
    assertFalse(board.isValidMove("0 2", new Tile(Type.DIAMOND, Color.BLUE), newTiles, silent));
    assertFalse(board.isValidMove("0 2", new Tile(Type.DIAMOND, Color.GREEN), newTiles, silent));
    assertFalse(board.isValidMove("0 2", new Tile(Type.DIAMOND, Color.YELLOW), newTiles, silent));
    
    assertTrue(board.isValidMove("0 -2", new Tile(Type.DIAMOND, Color.ORANGE), newTiles, silent));
    assertTrue(board.isValidMove("0 -2", new Tile(Type.DIAMOND, Color.PURPLE), newTiles, silent));
    assertTrue(board.isValidMove("0 -2", new Tile(Type.DIAMOND, Color.RED), newTiles, silent));
    assertFalse(board.isValidMove("0 -2", new Tile(Type.RECTANGLE, Color.ORANGE), newTiles, 
        silent));
    assertFalse(board.isValidMove("0 -2", new Tile(Type.CLUBS, Color.PURPLE), newTiles, silent));
    assertFalse(board.isValidMove("0 -2", new Tile(Type.STAR, Color.RED), newTiles, silent));
    assertFalse(board.isValidMove("0 -2", new Tile(Type.DIAMOND, Color.BLUE), newTiles, silent));
    assertFalse(board.isValidMove("0 -2", new Tile(Type.DIAMOND, Color.GREEN), newTiles, silent));
    assertFalse(board.isValidMove("0 -2", new Tile(Type.DIAMOND, Color.YELLOW), newTiles, silent));
  }
}
