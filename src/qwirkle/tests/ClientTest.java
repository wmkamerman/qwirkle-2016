package qwirkle.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import qwirkle.network.Client;

/**
 * The Class ClientTest.
 */
public class ClientTest {
  
  /** The client. */
  private Client client;
  
  /** The Constant NAME. */
  private static final String NAME = "Wim";
  
  /**
   * Make sure you start a server on port PORT.
   */
  @Before
  public void setUp() {
    client = new Client(NAME, true, 100);
  }
  
  /**
   * Test initializing.
   */
  @Test
  public void testInitial() {
    assertEquals(NAME, client.getName());
    assertEquals("0 0 0 0", client.getSupportedFeatures());
  }
  
}
