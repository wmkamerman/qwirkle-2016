package qwirkle.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import qwirkle.controller.GameController;
import qwirkle.model.AbstractPlayer;

import qwirkle.model.LocalPlayer;
import qwirkle.model.Tile;
import qwirkle.model.Tile.Color;
import qwirkle.model.Tile.Type;

import java.util.ArrayList;

/**
 * The Class AbstractPlayerTest.
 */
public class AbstractPlayerTest {
  
  /** The player. */
  private AbstractPlayer player;
  
  /** The game. */
  private GameController game;
  
  /** The Constant PLAYERNAME. */
  private static final String PLAYERNAME = "Wim"; 
  
  /**
   * Create a new LocalPlayer to test the implementation of the AbstractPlayer.
   * LocalPlayer needs to be created because the AbstractPlayer cannot be instantiated.
   */
  @Before
  public void setUp() {
    game = new GameController(false);
    player = new LocalPlayer(PLAYERNAME, game);
  }
  
  /**
   * Check if the construction of a new player works as expected.
   * Name and score should be set. The inventory should be initialized and empty.
   */
  @Test
  public void testInitial() {
    assertEquals(PLAYERNAME, player.getName());
    assertEquals(0, player.getScore());
    assertEquals(new ArrayList<Tile>(), player.getInventory());
    assertEquals(0, player.getInventorySize());
  }
  
  /**
   * Test the inventory of the player by adding and removing tiles from the inventory.
   */
  @Test
  public void testInventory() {
    // Make new tile. Should not be in the inventory.
    Tile t1 = new Tile(Type.CROSS, Color.BLUE); 
    assertFalse(player.getInventory().contains(t1));
    
    // Tile should be in the inventory after adding Tile to the inventory
    player.addTile(t1);
    assertTrue(player.getInventory().contains(t1));
    
    // Tile should not be in the inventory anymore after removing Tile from the inventory
    player.removeTile(t1);
    assertFalse(player.getInventory().contains(t1));
  }
  
  /**
   * Test the newTiles Tile list of the player by adding and removing tiles, and clearing the list. 
   */
  @Test
  public void testNewTilesList() {
    Tile t1 = new Tile(Type.CROSS, Color.BLUE);
    Tile t2 = new Tile(Type.DIAMOND, Color.YELLOW);
    
    assertEquals(0, player.getNewTiles().size());
    assertFalse(player.getNewTiles().containsKey(t1));
    assertFalse(player.getNewTiles().containsKey(t2));
    
    // Add both tiles to the list
    player.addToNewTiles(t1, "0 0");
    player.addToNewTiles(t2, "0 1");
    assertTrue(player.getNewTiles().containsKey(t1));
    assertTrue(player.getNewTiles().containsKey(t2));
    
    // Both tiles should not be in the list anymore after clearing the list
    player.clearNewTiles();
    assertFalse(player.getNewTiles().containsKey(t1));
    assertFalse(player.getNewTiles().containsKey(t2));
    
    // The list should be empty after clearing it as well.
    assertEquals(0, player.getNewTiles().size());
  }
  
  /**
   * Test the score functionality of the player: initial score, adding scores 
   * and clearing the score.
   */
  @Test
  public void testScore() {
    // Check that score is indeed 0 after initialization
    assertEquals(0, player.getScore());
    
    // After adding 10 to the score, the score should be 10.
    int scoreToAdd = 10;
    player.addToScore(scoreToAdd);
    assertEquals(10, player.getScore());
    
    // After adding 10 to the score another time, the score should be 20.
    player.addToScore(scoreToAdd);
    assertEquals(20, player.getScore());
    
    // After clearing the score, the score should be 0 again.
    player.clearScore();
    assertEquals(0, player.getScore());
  }
  
  /**
   * Test the toString method, according to the expected result described in JavaDoc.
   */
  @Test
  public void testToString() {
    Tile t1 = new Tile(Type.CROSS, Color.BLUE);
    Tile t2 = new Tile(Type.DIAMOND, Color.YELLOW);
    
    player.addTile(t1);
    player.addTile(t2);
    
    String expected = t1.toString() + " | " + t2.toString();
    assertEquals(expected, player.toString());
  }
  
  /**
   * Test the Hint functionality. A hint should be returned as a random item from a list 
   * of possible moves.
   * So the hint must contain a ToString from one of the tiles from the inventory.
   * If no valid move is found, the Hint should not contain such a string.
   */
  @Test
  public void testHint() {
    // First add two tiles to inventory
    Tile t1 = new Tile(Type.CROSS, Color.BLUE);
    Tile t2 = new Tile(Type.DIAMOND, Color.YELLOW);
    player.addTile(t1);
    player.addTile(t2);
    
    // Hint should contain one of the toString methods of the tiles with an empty board.
    String s1 = t1.toWords();
    String s2 = t2.toWords();
    
    String hint = player.getHint(game.getBoardObject());
    System.out.println(hint);
    assertTrue(
        hint.contains(s1) || hint.contains(s2)
    );
    
    // Hint must return that there is no valid move when board only has tile
    // that the two inventory tiles cannot be placed next to.
    Tile t3 = new Tile(Type.CIRCLE, Color.PURPLE);
    game.setMove("0 0", t3, player, false);
    assertEquals(1, game.getBoardObject().getTilesOnBoard().size());
    
    String newHint = player.getHint(game.getBoardObject());
    assertFalse(
        newHint.contains(s1) || newHint.contains(s2)
    );
    
  }
  
  
}
