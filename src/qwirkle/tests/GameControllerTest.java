package qwirkle.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import qwirkle.controller.GameController;
import qwirkle.model.LocalPlayer;
import qwirkle.model.Player;
import qwirkle.model.Tile;


/**
 * The Class GameControllerTest.
 */
public class GameControllerTest {
  
  /** The game. */
  private GameController game;
  
  /**
   * Instantiate the GameController.
   */
  @Before
  public void setUp() {
    game = new GameController(false);
  }
  
  /**
   * Check if board is empty on initialization.
   */
  @Test
  public void testInitial() {
    assertEquals("", game.getBoard());
    assertEquals(108, game.getDeckSize());
    assertNull(game.getCurrentPlayer());
  }
  
  /**
   * Tests adding players to the game.
   */
  @Test
  public void testAddPlayer() {
    Player player = new LocalPlayer("Wim", game);
    assertEquals(0, player.getInventorySize());
    game.addPlayer(player);
    assertEquals(GameController.INVENTORY_SIZE, player.getInventorySize());
    assertEquals(GameController.INVENTORY_SIZE, game.getInventory(player).size());
    assertEquals(player, game.getCurrentPlayer());
  }
  
  /**
   * Test the setMove function by adding a player, placing a Tile at "0 0".
   * The tile should be added to the players' newTiles list, and removed from its inventory.
   * If the move is invalid, this should not be the case.
   */
  @Test
  public void testSetMove() {
    Player player = new LocalPlayer("Wim", game);
    game.addPlayer(player);
    
    // Valid move
    Tile t1 = player.getInventory().get(0);
    game.setMove("0 0", t1, player, false);
    assertFalse(player.getInventory().contains(t1));
    assertTrue(player.getNewTiles().containsKey(t1));
    
    // Invalid move
    Tile t2 = player.getInventory().get(0);
    game.setMove("0 5", t2, player, true);
    assertTrue(player.getInventory().contains(t2));
    assertFalse(player.getNewTiles().containsKey(t2));
  }
  
}
