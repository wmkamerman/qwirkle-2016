package qwirkle.view;

import qwirkle.controller.GameController;
import qwirkle.controller.ViewController;
import qwirkle.model.ComputerPlayer;
import qwirkle.model.LocalPlayer;
import qwirkle.model.Player;
import qwirkle.model.Tile;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Observable;
import java.util.Observer;
import java.util.Scanner;

/**
 * The Class QwirkleTUI is a Textual User Interface for the game.
 */
public class QwirkleTui implements Observer, ViewController {

  /** The gamecontroller. */
  private GameController gamecontroller;
  
  /** The location to send messages to. */
  private PrintStream out = System.out;
  
  /** The location to send error messages to. */
  private PrintStream err = System.err;
  
  /** Whether or not the TUI should set up a (local) game. */
  private boolean hasTuiSetUp;

  /**
   * Instantiates a new Qwirkle TUI.
   * @param controller the gamecontroller controlling the TUI
   * @param hasTuiSetUp Whether or not the TUI should set up a (local) game
   */
  public QwirkleTui(GameController controller, boolean hasTuiSetUp) {
    this.hasTuiSetUp = hasTuiSetUp;
    gamecontroller = controller;
  }

  /* (non-Javadoc)
   * @see qwirkle.controller.ViewController#start()
   */
  @Override
  public void start() {
    out.println("Welkom bij Qwirkle!");

    if (hasTuiSetUp) {

      Scanner in = new Scanner(System.in);

      int amount = 0;
      String init = null;
      showMsg("Met hoeveel spelers wil je dit spel spelen? (2-8)");
      while (init == null || amount == 0) {
        init = in.hasNextLine() ? in.nextLine() : null;
        if (init != null) {
          int aa;
          try {
            aa = Integer.parseInt(init);
          } catch (NumberFormatException e) {
            aa = 0;
          }
          if (2 <= aa && aa <= 8) {
            amount = aa;
          } else {
            showError("geef een getal tussen 2 en 8 op!");
          }
        }
      }

      for (int i = 0; i < amount; i++) {
        showMsg("Geef een naam op voor speler " + (i + 1) + ":");
        String name;
        name = in.hasNextLine() ? in.nextLine() : null;
        if (name != null) {
          showMsg("Is " + name + " een computerspeler? (Y/N)");
          String type;
          type = in.hasNextLine() ? in.nextLine() : null;
          if (type.equals("Y")) {
            addPlayer(new ComputerPlayer(name, gamecontroller));
          } else if (type.equals("N")) {
            addPlayer(new LocalPlayer(name, gamecontroller));
          }
        } else {
          i--;
        }
      }

      showMsg("\nReady to start!");
      showBoard(getBoard());
      Player pl = getCurrentPlayer();
      if (!(pl instanceof ComputerPlayer)) {
        showInventory(getInventory(), pl.getName());
      }
      showMsg("\nVoer 'HELP' in om de commando's te bekijken!");
      gamecontroller.play();

      in.close();
    }
  }

  // -------- SET FUNCTIONS ---------

  /**
   * Sets a move.
   * @param coords the coords of the move
   * @param tile the tile to be placed
   * @param player the player that places the move
   * @param silent whether or not there should be visual feedback on the setmove.
   */
  public void setMove(String coords, Tile tile, Player player, boolean silent) {
    gamecontroller.setMove(coords, tile, player, silent);
    showBoard(getBoard());
    showInventory(getInventory(), player.getName());
  }

  /**
   * Adds a player to the game.
   * @param player the player to add
   */
  public void addPlayer(Player player) {
    gamecontroller.addPlayer(player);
  }

  /**
   * Mark the end of a turn.
   */
  public void endOfTurn() {
    gamecontroller.endOfTurn();
    showNextPlayer();
    showBoard(getBoard());
    showInventory(getInventory(), getCurrentPlayer().getName());
  }

  /**
   * Do a trade.
   * @param tilesToTrade the tiles to trade
   */
  public void doTrade(HashSet<Tile> tilesToTrade) {
    gamecontroller.doTrade(tilesToTrade);
    showMsg("Je hebt succesvol " + tilesToTrade.size() + " stenen gewisseld.");
    showNextPlayer();
    showBoard(getBoard());
    showInventory(getInventory(), getCurrentPlayer().getName());
  }

  // -------- GET FUNCTIONS ---------

  /**
   * Gets the board.
   * @return the board
   */
  public String getBoard() {
    return gamecontroller.getBoard();
  }

  /**
   * Gets the inventor as a list of tiles.
   * @return the inventory
   */
  public ArrayList<Tile> getInventory() {
    return gamecontroller.getInventory(getCurrentPlayer());
  }

  /**
   * Gets the deck size.
   * @return the deck size
   */
  public int getDeckSize() {
    return gamecontroller.getDeckSize();
  }

  /**
   * Gets the current player.
   * @return the current player
   */
  public Player getCurrentPlayer() {
    return gamecontroller.getCurrentPlayer();
  }

  /**
   * Gets the scores.
   * @return the scores
   */
  public HashMap<String, Integer> getScores() {
    return gamecontroller.getScores();
  }

  // -------- SHOW FUNCTIONS ---------

  /* (non-Javadoc)
   * @see qwirkle.controller.ViewController#showBoard(java.lang.String)
   */
  public void showBoard(String board) {
    out.println(board);
  }

  /* (non-Javadoc)
   * @see qwirkle.controller.ViewController#showNextPlayer()
   */
  public void showNextPlayer() {
    showMsg(getCurrentPlayer().getName() + " is aan de beurt!");
  }

  /* (non-Javadoc)
   * @see qwirkle.controller.ViewController#showInventory(java.util.ArrayList, java.lang.String)
   */
  @Override
  public void showInventory(ArrayList<Tile> inventory, String name) {
    out.println("Inventory van " + name + ":");
    for (int i = 0; i < inventory.size(); i++) {
      out.println("Tile " + (i + 1) + ": " + inventory.get(i).toWords());
    }
  }

  /* (non-Javadoc)
   * @see qwirkle.controller.ViewController#showError(java.lang.String)
   */
  public void showError(String error) {
    err.println("Error: " + error);
  }

  /* (non-Javadoc)
   * @see qwirkle.controller.ViewController#showMsg(java.lang.String)
   */
  public void showMsg(String msg) {
    out.println(msg);
  }

  /* (non-Javadoc)
   * @see qwirkle.controller.ViewController#showHelp()
   */
  @Override
  public void showHelp() {
    showMsg("De volgende commando's zijn beschikbaar:" + "\n- SETMOVE x y t (zet de tile met "
        + "nummer t op (x,y))"
        + "\n- SCORES (bekijk scores van alles spelers)" + "\n- NEXT (ga naar volgende speler)"
        + "\n- TRADE 1 3 5 om bijv. stenen 1 3 en 5 te wisselen" + "\n- EXIT (stop Qwirkle)");
  }

  /**
   * Show deck size.
   * @param size the size of the deck
   */
  public void showDeckSize(int size) {
    out.println(size);
  }

  /* (non-Javadoc)
   * @see qwirkle.controller.ViewController#showScores(java.util.HashMap)
   */
  @Override
  public void showScores(HashMap<String, Integer> scores) {
    for (String name : scores.keySet()) {
      showMsg(name + ": " + scores.get(name));
    }
  }

  /* (non-Javadoc)
   * @see java.util.Observer#update(java.util.Observable, java.lang.Object)
   */
  @Override
  public void update(Observable obj, Object arg) {
    out.println(arg);
  }

}
