package qwirkle.model;

/**
 * The Class Tile defines a tile and its properties.
 */
public class Tile {

  /** The type of the tile. */
  public Type type;
  
  /** The color of the tile. */
  public Color color;

  /**
   * Instantiates a new tile.
   * @param type the type of the tile
   * @param color the color of the tile
   */
  public Tile(Type type, Color color) {
    this.type = type;
    this.color = color;
  }

  /**
   * Gets the color of the tile.
   * @return the color
   */
  public Color getColor() {
    return color;
  }

  /**
   * Gets the type of the tile.
   * @return the type
   */
  public Type getType() {
    return type;
  }

  /**
   * The Type of the tile.
   */
  public enum Type {
    
    /** The circle type. */
    CIRCLE(1, "\u25CF"), 
    /** The cross type. */
    CROSS(2, "\u2716"), 
    /** The diamond type. */
    DIAMOND(3, "\u25C6"), 
    /** The rectangle type. */
    RECTANGLE(4, "\u25A0"), 
    /** The star type. */
    STAR(5, "\u2605"), 
    /** The clubs type. */
    CLUBS(6, "\u2663");

    /** The nr. */
    public int nr;
    
    /** The symbol. */
    public String symbol;

    /**
     * Instantiates a new type.
     * @param color the color
     * @param type the type
     */
    Type(int color, String type) {
      this.nr = color;
      this.symbol = type;
    }
  }

  /**
   * The Enum Color.
   */
  public enum Color {
    
    /** The red. */
    RED(1, "\u001B[31m"), 
   /** The orange. */
   ORANGE(2, "\u001B[37m"), 
   /** The yellow. */
   YELLOW(3, "\u001B[33m"), 
      
      /** The green. */
      GREEN(4, "\u001B[32m"), 
   /** The blue. */
   BLUE(5, "\u001B[36m"), 
   /** The purple. */
   PURPLE(6, "\u001B[35m");

    /** The nr. */
    public int nr;
    
    /** The colorcode. */
    public String colorcode;

    /**
     * Instantiates a new color.
     * @param colornumber the colornumber
     * @param colorcode the colorcode
     */
    Color(int colornumber, String colorcode) {
      this.nr = colornumber;
      this.colorcode = colorcode;
    }
  }

  /**
   * Return this tile as a colored symbol.
   * @return the tile as a colored symbol
   */
  public String toWords() {
    return this.color.colorcode + this.type.symbol + "\u001B[0m";
  }

  /**
   * Return this tile a type number, color number and its symbol.
   * @return this tile a type number, color number and its symbol
   */
  public String toString() {
    return String.valueOf(this.type.nr) + " " + String.valueOf(this.color.nr) + " " 
        + this.type.symbol;
  }

  /**
   * Return this tile as a type number and a color number as defined by protocol.
   * @return the tile as a type number and a color number
   */
  public String toNetwork() {
    return String.valueOf(this.type.nr) + " " + String.valueOf(this.color.nr);
  }

  /**
   * Test class.
   * @param args Not needed
   */
  public static void main(String[] args) {
    Type s1 = Type.CROSS;
    Color t1 = Color.BLUE;
    Tile testTile = new Tile(s1, t1);
    System.out.println(testTile.toWords());
    System.out.println(testTile.toString());
    System.out.println(testTile.toNetwork());
    System.out.println(testTile.getColor());
    System.out.println(testTile.getType());
  }

}
