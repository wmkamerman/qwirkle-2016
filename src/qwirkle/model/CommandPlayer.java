package qwirkle.model;

import qwirkle.controller.NetworkController;
import qwirkle.network.ClientHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class CommandPlayer is a NetworkPlayer used by the Server.
 */
public class CommandPlayer extends NetworkPlayer {

  /** The clienthandler that controls the player. */
  private ClientHandler clientHandler;

  /**
   * Instantiates a new command player.
   * @param name the name of the player
   * @param controller the controller that controls this Player
   * @param client the clienthandler connected to this player
   */
  public CommandPlayer(String name, NetworkController controller, ClientHandler client) {
    super(name, controller, false, 0);
    this.clientHandler = client;
  }

  /**
   * Add a list of tiles to players' inventory.
   * @param tilesToAdd List of tiles to add to players' inventory
   */
  public void fillInventory(List<Tile> tilesToAdd) {
    for (Tile t : tilesToAdd) {
      this.addTile(t);
    }
  }

  /* (non-Javadoc)
   * @see qwirkle.model.AbstractPlayer#removeTile(qwirkle.model.Tile)
   */
  @Override
  public void removeTile(Tile tile) {
    List<Tile> tilesToRemove = new ArrayList<Tile>();
    for (Tile t : inventory) {
      if (t.color.equals(tile.color) && t.type.equals(tile.type)) {
        tilesToRemove.add(t);
        break;
      }
    }
    for (Tile t : tilesToRemove) {
      inventory.remove(t);
    }
  }

  /**
   * Gets the clienthandler.
   * @return the client handler belonging to this player.
   */
  public ClientHandler getClientHandler() {
    return clientHandler;
  }

}
