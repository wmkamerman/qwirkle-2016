package qwirkle.model;

import qwirkle.controller.GameController;
import qwirkle.model.Tile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;

/**
 * Implementation of Player interface, base for all player types.
 * @author wimkamerman
 */
public abstract class AbstractPlayer implements Player {

  /** The inventory. */
  protected ArrayList<Tile> inventory;
  
  /** The tiles placed during a move. */
  protected LinkedHashMap<Tile, String> newTiles = new LinkedHashMap<Tile, String>();
  
  /** The name of the player. */
  private String name;
  
  /** The score of the player. */
  private int score;
  
  /** The gamecontroller that controls this player. */
  protected GameController gamecontroller;

  /**
   * Constructs an AbstractPlayer. Resets score, sets name and controller.
   * Initializes inventory.
   * @param name Name of AbstractPlayer
   * @param controller GameController object which controls this AbstractPlayer.
   */
  public AbstractPlayer(String name, GameController controller) {
    this.name = name;
    this.score = 0;
    this.gamecontroller = controller;
    inventory = new ArrayList<Tile>();
  }

  /* (non-Javadoc)
   * @see qwirkle.model.Player#getName()
   */
  @Override
  public String getName() {
    return name;
  }

  /* (non-Javadoc)
   * @see qwirkle.model.Player#getInventory()
   */
  @Override
  public ArrayList<Tile> getInventory() {
    return inventory;
  }

  /* (non-Javadoc)
   * @see qwirkle.model.Player#getInventorySize()
   */
  @Override
  public int getInventorySize() {
    return inventory.size();
  }

  /* (non-Javadoc)
   * @see qwirkle.model.Player#getScore()
   */
  @Override
  public int getScore() {
    return score;
  }

  /* (non-Javadoc)
   * @see qwirkle.model.Player#addTile(qwirkle.model.Tile)
   */
  @Override
  public void addTile(Tile tile) {
    inventory.add(tile);
  }

  /* (non-Javadoc)
   * @see qwirkle.model.Player#removeTile(qwirkle.model.Tile)
   */
  @Override
  public void removeTile(Tile tile) {
    if (inventory.contains(tile)) {
      inventory.remove(tile);
    }
  }

  /* (non-Javadoc)
   * @see qwirkle.model.Player#addToNewTiles(qwirkle.model.Tile, java.lang.String)
   */
  @Override
  public void addToNewTiles(Tile tile, String coords) {
    newTiles.put(tile, coords);
  }

  /* (non-Javadoc)
   * @see qwirkle.model.Player#getNewTiles()
   */
  @Override
  public LinkedHashMap<Tile, String> getNewTiles() {
    return newTiles;
  }

  /* (non-Javadoc)
   * @see qwirkle.model.Player#clearNewTiles()
   */
  @Override
  public void clearNewTiles() {
    newTiles.clear();
  }

  /* (non-Javadoc)
   * @see qwirkle.model.Player#addToScore(int)
   */
  @Override
  public void addToScore(int number) {
    score += number;
  }
  
  /* (non-Javadoc)
   * @see qwirkle.model.Player#clearScore()
   */
  @Override
  public void clearScore() {
    score = 0;
  }
  
  /**
   * Creates a string representing the players' inventory.
   * @return inventory as string
   */
  @Override
  public String toString() {
    String result = "";
    int size = getInventorySize();
    for (int i = 0; i < size; i++) {
      result += inventory.get(i).toString();
      if ((i + 1) < size) {
        // not last iteration, so add the " | " at the end
        result += " | ";
      }
    }
    return result;
  }

  /* (non-Javadoc)
   * @see qwirkle.model.Player#determineMove(qwirkle.model.Board)
   */
  public abstract void determineMove(Board board);

  /**
   * Sets a move at certain coordinates.
   * @param coords the coords of the new tile
   * @param tile the tile to place
   * @param player the player
   * @param silent whether or not anything should be printed.
   */
  public void setMove(String coords, Tile tile, Player player, boolean silent) {
    gamecontroller.setMove(coords, tile, player, silent);
  }

  /**
   * Mark the end of a turn.
   */
  public void endOfTurn() {
    gamecontroller.endOfTurn();
  }
  
  /**
   * Returns a hint for the player as a String.
   * @param board Board object of the game.
   * @return Hint for the player, based on possible moves and the inventory of the player
   */
  public String getHint(Board board) {
    HashMap<String, Tile> possibleMoves = getPossibleValidMoves(board);
    String hint = "";
    if (possibleMoves.size() > 0) {
      Random random = new Random();
      List<String> keys = new ArrayList<String>(possibleMoves.keySet());
      String randomKey = keys.get(random.nextInt(keys.size()));
      hint = "Je kunt " + possibleMoves.get(randomKey).toWords() + " op coordinaten " 
          + randomKey + " plaatsen.";
    } else {
      hint = "Helaas kun je geen geldige zet plaatsen op dit moment.";
    }
    return hint;
  }
  
  /**
   * Return a list of possible valid moves based on allowed coordinates and 
   * tiles in the inventory of the player.
   * @param board Board object of the game.
   * @return List of possible valid moves as HashMap with Coordinates, and Tile object
   */
  public HashMap<String, Tile> getPossibleValidMoves(Board board) {
    ArrayList<String> allowedCoords = getAllowedCoords(board);
    HashMap<String, Tile> possibleMoves = new HashMap<String, Tile>();

    for (Tile t : getInventory()) {
      for (String coords : allowedCoords) {
        if (board.isValidMove(coords, t, newTiles, true)) {
          possibleMoves.put(coords, t);
        }
      }
    }
    
    return possibleMoves;
  }
  
  /**
   * Get a list of all coordinates which adjoin current tiles on the board. 
   * @param board Board object to get coordinates from
   * @return List of Coordinates as string
   */
  public ArrayList<String> getAllowedCoords(Board board) {
    HashMap<String, Tile> tilesToCheck = new HashMap<String, Tile>();
    
    if (!newTiles.isEmpty()) {
      for (Entry<Tile, String> entry : newTiles.entrySet()) {
        tilesToCheck.put(entry.getValue(), entry.getKey());
      }
    } else {
      tilesToCheck = board.getTilesOnBoard();
    }

    ArrayList<String> allowedCoords = new ArrayList<String>();
    if (tilesToCheck.isEmpty()) {
      allowedCoords.add("0 0");
    } else {
      for (String coords : tilesToCheck.keySet()) {
        int xcoord = board.getX(coords);
        int ycoord = board.getY(coords);
        
        String coorda = (xcoord + 1) + " " + ycoord;
        if (!tilesToCheck.containsKey(coorda) && !allowedCoords.contains(coorda)) {
          allowedCoords.add(coorda);
        }
        
        String coordb = xcoord + " " + (ycoord + 1);
        if (!tilesToCheck.containsKey(coordb) && !allowedCoords.contains(coordb)) {
          allowedCoords.add(coordb);
        }
        
        String coordc = (xcoord - 1) + " " + ycoord;
        if (!tilesToCheck.containsKey(coordc) && !allowedCoords.contains(coordc)) {
          allowedCoords.add(coordc);
        }
        
        String coordd = xcoord + " " + (ycoord - 1);
        if (!tilesToCheck.containsKey(coordd) && !allowedCoords.contains(coordd)) {
          allowedCoords.add(coordd);
        }
      }
    }
    return allowedCoords;
  }

}
