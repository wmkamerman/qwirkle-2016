package qwirkle.model;

import qwirkle.controller.GameController;

import java.util.HashSet;
import java.util.Scanner;

/**
 * The Class LocalPlayer is a human-controlled local player.
 */
public class LocalPlayer extends AbstractPlayer {

  /**
   * Instantiates a new local player.
   * @param name the name of the player
   * @param controller the controller that controls this player
   */
  public LocalPlayer(String name, GameController controller) {
    super(name, controller);
  }

  /* (non-Javadoc)
   * @see qwirkle.model.AbstractPlayer#determineMove(qwirkle.model.Board)
   */
  @Override
  public void determineMove(Board board) {

    Scanner in = new Scanner(System.in);

    loop: while (in.hasNextLine()) {
      Scanner inputline = new Scanner(in.nextLine());
      if (inputline.hasNext()) {
        String command = inputline.next();
        switch (command) {
          case "NEXT":
            // endOfTurn() will be called within the Play() method in GameController
            break loop;
          case "SCORES":
            gamecontroller.showScores();
            break;
          case "SETMOVE":
            if (inputline.hasNext()) {
              int xcoord = Integer.parseInt(inputline.next());
              int ycoord = Integer.parseInt(inputline.next());
              int tileNumber = Integer.parseInt(inputline.next()) - 1;
              setMove(xcoord + " " + ycoord, getInventory().get(tileNumber), this, false);
            }
            break;
          case "TRADE":
            if (newTiles.size() == 0) {
              HashSet<Tile> tilesToTrade = new HashSet<Tile>();
              while (inputline.hasNext()) {
                tilesToTrade.add(getInventory().get(Integer.parseInt(inputline.next()) - 1));
              }
              if (tilesToTrade.size() > 0) {
                if (tilesToTrade.size() <= gamecontroller.getDeckSize()) {
                  gamecontroller.doTrade(tilesToTrade);
                  break loop;
                } else {
                  gamecontroller.showMsg("Er zijn niet genoeg stenen in de zak. (" 
                      + gamecontroller.getDeckSize() + ")");
                }
              } else {
                gamecontroller.showMsg("Mislukt. Voer in als: TRADE 2 4 6 bijv. "
                    + "om de tiles 2, 4 en 6 te wisselen.");
              }
            } else {
              gamecontroller.showMsg("Je hebt nu al een zet gedaan, cheater!");
            }
            break;
          case "HELP":
            gamecontroller.showHelp();
            break;
          default:
            gamecontroller.showMsg("Invalid command! Perhaps you need a break?");
            gamecontroller.showMsg("Voer 'HELP' in om alle commando's te bekijken");
            break;
          } // end switch
      } // end if
      inputline.close();
    } // end loop / while

    // in.close();
  }

}
