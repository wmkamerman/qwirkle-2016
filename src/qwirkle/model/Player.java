package qwirkle.model;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * The Interface Player provides a base for all players.
 */
public interface Player {

  // -------Queries-------

  /**
   * Gets the name.
   * @return the name of the player
   */
  public String getName();

  /**
   * Gets the inventory.
   * @return the inventory of this player
   */
  public ArrayList<Tile> getInventory();

  /**
   * Gets the score of this player.
   * @return the score of this player
   */
  public int getScore();

  /**
   * Gets the inventory size of the player.
   * @return the inventory size of the player
   */
  public int getInventorySize();

  // ------Commands-------

  /**
   * Adds a tile to the inventory of this player.
   * @param tile the tile to be added
   */
  public void addTile(Tile tile);

  /**
   * Removes a tile from the inventory of this player.
   * @param tile the tile to be removed
   */
  public void removeTile(Tile tile);

  /**
   * Adds a tile to the new tiles map.
   * @param tile the tile to be added
   * @param coords the coordinates of the tile
   */
  public void addToNewTiles(Tile tile, String coords);

  /**
   * Gets the new tiles map.
   * @return the new tiles placed by this player in its current move. 
   */
  public LinkedHashMap<Tile, String> getNewTiles();

  /**
   * Clear the new tiles map.
   */
  public void clearNewTiles();

  /**
   * Determine move. Implementation differs for different types of players.
   * @param board the board to place tiles on
   */
  public void determineMove(Board board);

  /**
   * Adds a certain number the to score.
   * @param number the number to be added to the score
   */
  public void addToScore(int number);
  
  /**
   * Clears the score of this player.
   */
  public void clearScore();
}
