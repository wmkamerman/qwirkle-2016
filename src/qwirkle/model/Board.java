package qwirkle.model;

import qwirkle.model.Tile;
import qwirkle.model.Tile.Color;
import qwirkle.model.Tile.Type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Observable;

/**
 * The Class Board saves all tiles placed, and checks moves for validity.
 */
public class Board extends Observable {

  /** The minimum x-coordinate of the tiles placed. */
  private /*@ spec_public @*/ int minX;
  
  /** The maximum x-coordinate of the tiles placed. */
  private /*@ spec_public @*/ int maxX;
  
  /** The minimum y-coordinate of the tiles placed. */
  private /*@ spec_public @*/ int minY;
  
  /** The maximum y-coordinate of the tiles placed. */
  private /*@ spec_public @*/ int maxY;

  /** 
   * The tiles on board: Coordinates, Tile object.
   * Coordinates format: "x_space_y"
   */
  private /*@ spec_public @*/ HashMap<String, Tile> tilesOnBoard;

  /**
   * Constructs a board with new HashMap for tilesOnBoard, and clears that map.
   * Initializes borders of the map.
   */
  public Board() {
    tilesOnBoard = new HashMap<String, Tile>();
    clear();

    minX = 0;
    maxX = 0;
    minY = 0;
    maxY = 0;
  }

  /**
   * Clear the map of tiles of this board.
   */
  //@ ensures getTilesOnBoard().size() == 0;
  public void clear() {
    tilesOnBoard.clear();
  }

  /**
   * Adjusts board borders to new coordinates. 
   * @param coords Coordinates to adjust min and max with ("x y") 
   */
  //@ requires getX(coords) != null && getY(coords) != null;
  //@ ensures this.maxX == Math.max(getX(coords), this.maxX);
  //@ ensures this.minX == Math.min(getX(coords), this.minX);
  //@ ensures this.maxY == Math.max(getY(coords), this.maxY);
  //@ ensures this.minY == Math.min(getY(coords), this.minY);
  public void setBorders(String coords) {
    int xcoord = getX(coords);
    int ycoord = getY(coords);
    this.maxX = Math.max(xcoord, maxX);
    this.minX = Math.min(xcoord, minX);
    this.maxY = Math.max(ycoord, maxY);
    this.minY = Math.min(ycoord, minY);
  }

  /**
   * Set move: save move in the tilesOnBoard map.
   * @param coords Coordinates of tile
   * @param tile Tile object to place
   * @param silent Whether or not the observers should be notified
   */
  //@ requires getX(coords) != null && getY(coords) != null;
  //@ ensures getTilesOnBoard().size() == \old(getTilesOnBoard().size()) + 1;
  //@ ensures getTilesOnBoard().get(coords) == tile;
  public void setMove(String coords, Tile tile, boolean silent) {
    
    setChanged();
    if (tilesOnBoard.size() == 0) {
      coords = "0 0";
    }
    if (!tilesOnBoard.containsKey(coords)) {
      tilesOnBoard.put(coords, tile);
      setBorders(coords);
      if (!silent) {
        notifyObservers("setMove succesful");
      }
    } else {
      if (!silent) {
        notifyObservers("setMove failed.");
      }
    }
  }

  /**
   * Get x-coordinate of coords string (key in tilesOnBoard).
   * @param coords Coordinates string to get x-coordinate from
   * @return x-coordinate as integer, null if no integer
   */
  //@ ensures \result instanceof java.lang.Integer;
  public Integer getX(String coords) {
    String[] parts = coords.split(" ");
    try {
      return Integer.parseInt(parts[0]); 
    } catch (NumberFormatException e) {
      return null;
    }
  }

  /**
   * Get y-coordinate of coords string (key in tilesOnBoard).
   * @param coords Coordinates string to get y-coordinate from
   * @return y-coordinate as integer, null if no integer
   */
  //@ ensures \result instanceof java.lang.Integer;
  public Integer getY(String coords) {
    String[] parts = coords.split(" ");
    try {
      return Integer.parseInt(parts[1]); 
    } catch (NumberFormatException e) {
      return null;
    }
  }

  /**
   * Gets tilesOnBoard as hashmap.
   * @return HashMap with all tiles on board
   */
  //@ ensures \result instanceof HashMap;
  //@ ensures \result == tilesOnBoard;
  public HashMap<String, Tile> getTilesOnBoard() {
    return tilesOnBoard;
  }

  /**
   * Generate string of board to print in view with unicode icons.
   * @return String of board to print
   */
  //@ ensures \result == "";
  public String toPrint() {
    String result = "";
    // For each vertical row
    for (int y = (minY - 1); y <= (maxY + 1); y++) {
      // Print x-labels first row
      if (y == (minY - 1)) {
        System.out.printf("%2s", "");
        for (int x = (minX - 1); x <= (maxX + 1); x++) {
          System.out.printf("%4s", x);
        }
        System.out.printf("%4s%n", "  (x)");
      }

      // Print each coordinate
      System.out.printf("%5s", y + " ");
      for (int x = (minX - 1); x <= (maxX + 1); x++) {
        String coords = Integer.toString(x) + " " + Integer.toString(y);
        if (tilesOnBoard.containsKey(coords)) {
          String icon = tilesOnBoard.get(coords).type.symbol;
          String color = tilesOnBoard.get(coords).color.colorcode;
          System.out.printf("%-13s", color + icon + "\u001B[0m");
        } else {
          System.out.printf("%-4s", "-");
        }
      }
      System.out.print("\n");
    }

    System.out.printf("%4s%n", "  (y)");

    return result;
  }

  /**
   * Gets a list of all tiles that horizontally adjoin given coordinates.
   * @param coords Coordinates to get adjoining tiles from 
   * @return List of tiles that horizontally adjoin the coordinates, null if coordinate parse error 
   */
  //@ requires getX(coords) != null && getY(coords) != null;
  //@ ensures \result.contains(\forall int x; x >= getX(coords)+1 && x <= getX(coords)+6; tilesOnBoard.containsKey(Integer.toString(x) + " " + Integer.toString(getY(coords))));
  //@ ensures \result.contains(\forall int x2; x2 >= getX(coords)-6 && x2 <= getX(coords)-1; tilesOnBoard.containsKey(Integer.toString(x2) + " " + Integer.toString(getY(coords))));
  public ArrayList<Tile> getHorizontalTiles(String coords) {
    ArrayList<Tile> result = null;

    Integer xcoord = getX(coords);
    Integer ycoord = getY(coords);
    
    if (xcoord != null && ycoord != null) {
      
      result = new ArrayList<Tile>();
      
      // Get right tiles
      for (int i = (xcoord + 1); i <= (xcoord + 6); i++) {
        String tempcoord = Integer.toString(i) + " " + Integer.toString(ycoord);
        if (tilesOnBoard.containsKey(tempcoord)) {
          // Block to the right exists
          result.add(tilesOnBoard.get(tempcoord));
        } else {
          break;
        }
      }

      // Get left tiles
      for (int i = (xcoord - 1); i >= (xcoord - 6); i--) {
        String tempcoord = Integer.toString(i) + " " + Integer.toString(ycoord);
        if (tilesOnBoard.containsKey(tempcoord)) {
          // Block to the left exists
          result.add(tilesOnBoard.get(tempcoord));
        } else {
          break;
        }
      }
    }

    return result;
  }

  /**
   * Gets a list of all tiles that vertically adjoin given coordinates.
   * @param coords Coordinates to get adjoining tiles from 
   * @return List of tiles that vertically adjoin the coordinates, null if coordinate parse error
   */
  public ArrayList<Tile> getVerticalTiles(String coords) {
    ArrayList<Tile> result = null;
        
    Integer xcoord = getX(coords);
    Integer ycoord = getY(coords);
    
    if (xcoord != null && ycoord != null) {
      result = new ArrayList<Tile>();
      
      // Get down tiles
      for (int i = (ycoord + 1); i <= (ycoord + 6); i++) {
        String tempcoord = Integer.toString(xcoord) + " " + Integer.toString(i);
        if (tilesOnBoard.containsKey(tempcoord)) {
          // Block down exists
          result.add(tilesOnBoard.get(tempcoord));
        } else {
          break;
        }
      }

      // Get up tiles
      for (int i = (ycoord - 1); i >= (ycoord - 6); i--) {
        String tempcoord = Integer.toString(xcoord) + " " + Integer.toString(i);
        if (tilesOnBoard.containsKey(tempcoord)) {
          // Block up exists
          result.add(tilesOnBoard.get(tempcoord));
        } else {
          break;
        }
      }
    }
    
    return result;
  }

  /**
   * Check if a move is valid.
   * @param coords Coordinates of the move
   * @param ti Tile to be placed at coords
   * @param newTiles List of tiles that have been placed during current turn 
   * @param silent Whether or not the observers should be notified of possible errors.
   * @return Boolean if move is valid or not. True if valid, false if invalid.
   */
  public boolean isValidMove(String coords, Tile ti, LinkedHashMap<Tile, String> newTiles, 
      boolean silent) {

    boolean valid = false;
    String error = "";

    if (!tilesOnBoard.containsKey(coords)) {
      // There is no other tile at that place

      ArrayList<Tile> tilesHorizontal = new ArrayList<Tile>();
      ArrayList<Tile> tilesVertical = new ArrayList<Tile>();

      tilesHorizontal = getHorizontalTiles(coords);

      if (tilesHorizontal.size() < 6) {
        // Horizontal row length is valid

        tilesVertical = getVerticalTiles(coords);

        if (tilesVertical.size() < 6) {
          // Horizontal and Vertical row lengths are valid

          if ((tilesVertical.size() > 0 || tilesHorizontal.size() > 0) || tilesOnBoard.isEmpty()) {
            // Tile is next to another tile

            HashSet<String> newTilesCoords = new HashSet<String>();
            for (Tile newTile : newTiles.keySet()) {
              newTilesCoords.add(newTiles.get(newTile));
            }

            int xcoord = getX(coords);
            int ycoord = getY(coords);
            String coorda = (xcoord + 1) + " " + ycoord;
            String coordb = xcoord + " " + (ycoord + 1);
            String coordc = (xcoord - 1) + " " + ycoord;
            String coordd = xcoord + " " + (ycoord - 1);
            if (newTilesCoords.contains(coorda) || newTilesCoords.contains(coordb) 
                || newTilesCoords.contains(coordc) || newTilesCoords.contains(coordd) 
                || newTiles.isEmpty()) {
              // Tile is next to another placed tile in this turn

              // Save horizontal and vertical validity properties
              boolean validHorizontal = false;
              boolean validVertical = false;

              // Save tile-to-check properties
              Type theType = ti.type;
              Color theColor = ti.color;

              ArrayList<Color> existingHorizontalColors = new ArrayList<Color>();
              existingHorizontalColors.add(theColor);

              // Valid scenario 1: same type and different color
              horizontal1: for (Tile tile : tilesHorizontal) {
                // If type of every tile equals the new type
                if (tile.type.equals(theType)) {
                  Color thisColor = tile.color;
                  // If type is the same, check if color is different
                  if (existingHorizontalColors.contains(thisColor)) {
                    // Error in horizontal scenario 1: duplicate color
                    error += "Error in horizontal scenario 1: duplicate color!\n";
                    break horizontal1;
                  } else {
                    existingHorizontalColors.add(thisColor);
                  }
                } else {
                  // Error in horizontal scenario 1: types not the same
                  error += "Error in horizontal scenario 1: types not the same!\n";
                  break horizontal1;
                }
              }
              if ((existingHorizontalColors.size() - 1) == tilesHorizontal.size()) {
                // Valid horizontal scenario 1: same type and different color
                validHorizontal = true;
              }

              // Valid scenario 2: same color and different type
              if (!validHorizontal) {
                // Horizontal is not yet valid, so check scenario 2
                ArrayList<Type> existingHorizontalTypes = new ArrayList<Type>();
                existingHorizontalTypes.add(theType);

                horizontal2: for (Tile tile : tilesHorizontal) {
                  // If color of every tile equals the new color
                  if (tile.color.equals(theColor)) {
                    Type thisType = tile.type;
                    // If color is the same, check if type is different
                    if (existingHorizontalTypes.contains(thisType)) {
                      // Error in horizontal scenario 2: duplicate type
                      error += "Error in horizontal scenario 2: duplicate type!\n";
                      break horizontal2;
                    } else {
                      existingHorizontalTypes.add(thisType);
                    }
                  } else {
                    // Error in horizontal scenario 2: colors not the same
                    error += "Error in horizontal scenario 2: colors not the same!";
                    break horizontal2;
                  }
                }
                if ((existingHorizontalTypes.size() - 1) == tilesHorizontal.size()) {
                  // Valid horizontal scenario 2: same color and different type
                  error = ""; // If scenario 2 is valid but 1 isn't (and has written errors)
                  validHorizontal = true;
                }
              }

              // Now, if valid horizontal, check validity of vertical.
              if (validHorizontal) {
                ArrayList<Color> existingVerticalColors = new ArrayList<Color>();
                existingVerticalColors.add(theColor);

                // Valid vertical scenario 1: same type and different color
                vertical1: for (Tile tile : tilesVertical) {
                  // If type of every tile equals the new type
                  if (tile.type.equals(theType)) {
                    Color thisColor = tile.color;
                    // If type is the same, check if color is different
                    if (existingVerticalColors.contains(thisColor)) {
                      // Error in vertical scenario 1: duplicate color
                      error += "Error in vertical scenario 1: duplicate color!\n";
                      break vertical1;
                    } else {
                      existingVerticalColors.add(thisColor);
                    }
                  } else {
                    // Error in vertical scenario 1: types not the same
                    error += "Error in vertical scenario 1: types not the same!\n";
                    break vertical1;
                  }
                }
                if ((existingVerticalColors.size() - 1) == tilesVertical.size()) {
                  // Valid vertical scenario 1: same type and different color
                  validVertical = true;
                }

                if (!validVertical) {
                  ArrayList<Type> existingVerticalTypes = new ArrayList<Type>();
                  existingVerticalTypes.add(theType);

                  // Valid vertical scenario 2: same color and different type
                  vertical2: for (Tile tile : tilesVertical) {
                    // If color of every tile equals the new color
                    if (tile.color.equals(theColor)) {
                      Type thisType = tile.type;
                      // If color is the same, check if type is different
                      if (existingVerticalTypes.contains(thisType)) {
                        // Error in vertical scenario 2: duplicate type
                        error += "Error in vertical scenario 2: duplicate type!\n";
                        break vertical2;
                      } else {
                        existingVerticalTypes.add(thisType);
                      }
                    } else {
                      // Error in vertical scenario 2: colors not the same
                      error += "Error in vertical scenario 2: colors not the same!";
                      break vertical2;
                    }
                  }
                  if ((existingVerticalTypes.size() - 1) == tilesVertical.size()) {
                    // Valid vertical scenario 2: same color and different type
                    error = ""; // If scenario 2 is valid but 1 isn't (and has written errors)
                    validVertical = true;
                  }
                }
              }

              if (validHorizontal && validVertical) {
                valid = true;
              }
            } else {
              error += "Tile must be placed next to another placed tile!";
            }
          } else {
            error += "Tile must be placed next to another tile!";
          }
        } else {
          error += "Vertical row too long!";
        }
      } else {
        error += "Horizontal row too long!";
      }
    } else {
      error += "There already is a tile at that place!";
    }

    if (!silent) {
      setChanged();

      String result;
      if (valid) {
        result = "De zet is geldig!";
      } else {
        result = "Helaas... Deze zet is niet geldig. Het volgende ging mis:\n" + error;
      }

      notifyObservers(result);
    }

    return valid;
  }

}
