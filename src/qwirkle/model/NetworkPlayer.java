package qwirkle.model;

import qwirkle.controller.NetworkController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

/**
 * The Class NetworkPlayer provides network functionality for a player.
 */
public class NetworkPlayer extends AbstractPlayer {

  /** Whether or not this player is computer-controlled. */
  private boolean isComputer;
  
  /** The thinking time of the computerplayer. */
  private int thinkingTime;

  /**
   * Construct a NetworkPlayer.
   * @param name Name of this NetworkPlayer
   * @param controller Which NetworkController is controlling this Player.
   * @param isComputer Whether or not this Player is computer controlled.
   * @param thinkingTime Thinking time of this NetworkPlayer.
   */
  public NetworkPlayer(String name, NetworkController controller, boolean isComputer, 
      int thinkingTime) {
    super(name, controller);
    this.isComputer = isComputer;
    this.thinkingTime = thinkingTime;
  }

  /**
   * Determine move automatically if it is a computerplayer, else manually.
   * @param board Board to determine move on.
   */
  public void determineMove(Board board) {
    if (isComputer) {
      determineMoveComputer(board);
    } else {
      determineMoveManual(board);
    }
  }

  /**
   * Randomly place as many valid tiles as possible on the board.
   * Gets all possible coordinates to place tiles on, then generates a list of valid moves.
   * Then a valid move will be selected randomly and placed.
   * @param board The board to place the tiles on.
   */
  public synchronized void determineMoveComputer(Board board) {

    boolean continues = true;

    while (continues) {
      // Get allowed coordinates
      HashMap<String, Tile> possibleMoves = getPossibleValidMoves(board);

      if (possibleMoves.size() > 0) {
        Random random = new Random();
        List<String> keys = new ArrayList<String>(possibleMoves.keySet());
        String randomKey = keys.get(random.nextInt(keys.size()));
        try {
          this.wait(thinkingTime);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        setMove(randomKey, possibleMoves.get(randomKey), this, false);
      } else {
        System.out.println("Computerspeler kan niet (meer)");
        continues = false;
      }
    }

    endOfTurn();
  }

  /**
   * Manually place a move on the board with predefined commands.
   * User will be asked for input by System.in.
   * @param board Board to place tiles on.
   */
  public void determineMoveManual(Board board) {

    Scanner in = new Scanner(System.in);

    loop: while (in.hasNextLine()) {
      Scanner inputline = new Scanner(in.nextLine());
      if (inputline.hasNext()) {
        String command = inputline.next();
        switch (command) {
          case "NEXT":
            endOfTurn();
            break loop;
          case "SCORES":
            gamecontroller.showScores();
            break;
          case "SETMOVE":
            if (inputline.hasNext()) {
              try {
                int xcoord = Integer.parseInt(inputline.next());
                int ycoord = Integer.parseInt(inputline.next());
                int tileNumber = Integer.parseInt(inputline.next()) - 1;
                setMove(xcoord + " " + ycoord, getInventory().get(tileNumber), this, false);
              } catch (NumberFormatException e) {
                System.out.println("Ongeldige invoer. Probeer het opnieuw of kijk bij de HELP");
              }
            }
            break;
          case "TRADE":
            if (newTiles.size() == 0) {
              HashSet<Tile> tilesToTrade = new HashSet<Tile>();
              while (inputline.hasNext()) {
                tilesToTrade.add(getInventory().get(Integer.parseInt(inputline.next()) - 1));
              }
              if (tilesToTrade.size() > 0) {
                if (tilesToTrade.size() <= gamecontroller.getDeckSize()) {
                  gamecontroller.showMsg("Er worden " + tilesToTrade.size() + " stenen geruild.");
                  gamecontroller.doTrade(tilesToTrade);
                } else {
                  gamecontroller.showMsg("Er zijn niet genoeg stenen in de zak. (" 
                      + gamecontroller.getDeckSize() + ")");
                }
              } else {
                gamecontroller.showMsg("Mislukt. Voer in als: TRADE 2 4 6 bijv. "
                    + "om de tiles 2, 4 en 6 te wisselen.");
              }
            } else {
              gamecontroller.showMsg("Je hebt nu al een zet gedaan, cheater!");
            }
            break loop;
          case "HELP":
            gamecontroller.showHelp();
            break;
          case "HINT":
            gamecontroller.showMsg(getHint(board));
            break;
          default:
            gamecontroller.showMsg("Invalid command! Perhaps you need a break?");
            gamecontroller.showMsg("Voer 'HELP' in om alle commando's te bekijken");
            break;
          } // end switch
      } // end if
      inputline.close();
    } // end loop / while

  }

}
