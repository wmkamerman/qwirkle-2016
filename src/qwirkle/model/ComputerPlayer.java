package qwirkle.model;

import qwirkle.controller.GameController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 * The Class ComputerPlayer is a local computer-controlled player.
 */
public class ComputerPlayer extends AbstractPlayer {

  /**
   * Instantiates a new computer-controlled player.
   * @param name the name of the player
   * @param controller the controller that controls this player.
   */
  public ComputerPlayer(String name, GameController controller) {
    super(name, controller);
  }

  /* (non-Javadoc)
   * @see qwirkle.model.AbstractPlayer#determineMove(qwirkle.model.Board)
   */
  @Override
  public void determineMove(Board board) {

    boolean continues = true;

    while (continues) {
      // Get allowed coordinates
      ArrayList<String> allowedCoords = getAllowedCoords(board);
      HashMap<String, Tile> possibleMoves = new HashMap<String, Tile>();

      for (Tile t : getInventory()) {
        for (String coords : allowedCoords) {
          if (board.isValidMove(coords, t, newTiles, true)) {
            possibleMoves.put(coords, t);
          }
        }
      }

      if (possibleMoves.size() > 0) {
        Random random = new Random();
        List<String> keys = new ArrayList<String>(possibleMoves.keySet());
        String randomKey = keys.get(random.nextInt(keys.size()));
        gamecontroller.setMove(randomKey, possibleMoves.get(randomKey), this, true);
      } else {
        System.out.println("Computerspeler kan niet (meer)");
        continues = false;
      }
    }
  }

}
