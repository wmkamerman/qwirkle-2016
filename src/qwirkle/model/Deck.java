package qwirkle.model;

import java.util.ArrayList;
import java.util.Collections;

/**
 * The Class Deck controls the tiles in the game.
 */
public class Deck {

  /** The SIZE of the deck. */
  public static final int SIZE = 108;

  /** The tiles in the deck. */
  private static ArrayList<Tile> tiles;

  // ----------Constructors-------------

  /**
   * Creates a deck, consisting of 3 copies of each tile. There are 6 types and
   * 6 colors thus there are 108 tiles in the deck at the beginning of the game
   */
  public Deck() {
    tiles = new ArrayList<Tile>(SIZE);

    for (Tile.Type t : Tile.Type.values()) {
      for (Tile.Color c : Tile.Color.values()) {
        for (int i = 0; i < 3; i++) {
          Deck.tiles.add(new Tile(t, c));
        }
      }
    }

    shuffle();
  }

  // ---------Commands--------

  /**
   * Shuffle the deck.
   */
  private void shuffle() {
    Collections.shuffle(tiles);
  }

  /**
   * Draw a tile from the deck.
   * @return a tile from the deck 
   */
  public Tile drawTile() {
    return Deck.tiles.remove(0);
  }

  /**
   * Adds a tile to the deck then shuffles the deck.
   * @param tile the tile to be added
   */
  public void addTile(Tile tile) {
    Deck.tiles.add(tile);
    shuffle();
  }

  // --------Queries--------

  /**
   * Returns how many tiles are left in the deck.
   * @return the current size of the deck.
   */
  public int left() {
    return Deck.tiles.size();
  }

  /**
   * Checks if the deck is empty.
   * @return true, if deck is empty
   */
  public boolean isEmpty() {
    return tiles.isEmpty();
  }

  /**
   * Main test function.
   * @param args Not needed
   */
  public static void main(String[] args) {
    Deck deck = new Deck();
    System.out.println("Deck is empty: " + deck.isEmpty());
    Tile t1 = deck.drawTile();
    System.out.println("Drawn tile: " + t1.toString());
    System.out.println("Size of deck: " + deck.left());
    deck.addTile(t1);
    System.out.println("Size of deck after adding drawn tile: " + deck.left());
    
  }
}
