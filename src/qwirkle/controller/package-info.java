/**
 * Provides controller classes with game logic to connect models and views. 
 */
package qwirkle.controller;