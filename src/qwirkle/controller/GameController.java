package qwirkle.controller;

import qwirkle.model.Board;
import qwirkle.model.ComputerPlayer;
import qwirkle.model.Deck;
import qwirkle.model.Player;
import qwirkle.model.Tile;
import qwirkle.view.QwirkleTui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Observable;

/**
 * The main controller of Qwirkle: connects the models with the view and contains most game logic.
 * @author wimkamerman
 */
public class GameController extends Observable {

  /** The maximum inventory size for all players. */
  public static final int INVENTORY_SIZE = 6;
  /** Maximum amount of players to play a local game simultaneously. */
  public static final int MAX_PLAYERS = 8;

  /** The board. */
  protected Board board;
  
  /** The deck. */
  protected Deck deck;
  
  /** The viewcontroller. */
  protected ViewController view;
  
  /** The list of Players. */
  protected List<Player> player;

  /** Amount of players in the game. */
  protected int amountOfPlayers = 0;

  /** Index of current player in player[]. */
  protected int currentPlayer = 0;

  /**
   * Setup this GameController object by creating a new board, deck and an
   * empty player array.
   * 
   * @param hasTuiSetUp
   *      Whether or not the players will be set up by the TUI.
  */
  public GameController(boolean hasTuiSetUp) {
    board = new Board();
    deck = new Deck();
    player = new ArrayList<Player>();

    view = new QwirkleTui(this, hasTuiSetUp);
    board.addObserver(view);
  }

  // -------- GET FUNCTIONS ---------

  /**
   * Get board string to print.
   * @return String with visual representation of the board
   */
  public String getBoard() {
    return board.toPrint();
  }
  
  /**
   * Get board object.
   * @return board object that belongs to this game
   */
  public Board getBoardObject() {
    return board;
  }

  /**
   * Get inventory of a player as a list of tiles.
   * @param player Player object to get inventory from
   * @return ArrayList with inventory Tiles of Player p
   */
  public ArrayList<Tile> getInventory(Player player) {
    return player.getInventory();
  }

  /**
   * Get the amount of remaining tiles in the deck.
   * @return amount of remaining tiles in the deck
   */
  public int getDeckSize() {
    return deck.left();
  }

  /**
   * Get the Player object of the current player.
   * @return Player object of the current player, null if no players have been added.
   */
  public Player getCurrentPlayer() {
    Player result = null;
    if (currentPlayer < player.size()) {
      result = player.get(currentPlayer);
    }
    return result;
  }

  /**
   * Get scores of all players.
   * @return HashMap with names and scores of the players
   */
  public HashMap<String, Integer> getScores() {
    HashMap<String, Integer> result = new HashMap<String, Integer>();

    for (Player p : player) {
      result.put(p.getName(), p.getScore());
    }

    return result;
  }

  // -------- SET FUNCTIONS ---------

  /**
   * Add a new player to the player array.
   * @param pl Player object to be added to the game
   */
  public void addPlayer(Player pl) {
    if ((amountOfPlayers + 1) < MAX_PLAYERS) {
      amountOfPlayers++;
      player.add(pl);
      fillInventory(pl);
    }
  }

  /**
   * Set a move on the board.
   * @param coords Coordinates where the new tile needs to be placed ("x y")
   * @param tile Tile object that needs to be placed
   * @param player Player object of the player who placed the tile
   * @param silent Whether or not the board and inventory need to be showed after this setmove.
   */
  public void setMove(String coords, Tile tile, Player player, boolean silent) {
    if (board.isValidMove(coords, tile, player.getNewTiles(), silent)) {
      board.setMove(coords, tile, silent);
      player.addToNewTiles(tile, coords);
      calculateScore(player, silent);
      removeTileFromPlayer(tile, player);

      if (!silent) {
        view.showBoard(getBoard());
        if (!(player instanceof ComputerPlayer)) {
          view.showInventory(player.getInventory(), player.getName());
        }
      }
    }
  }

  /**
   * Get tile object from the deck.
   * @return Tile object drawn from the deck
   */
  public Tile drawTileFromDeck() {
    return deck.drawTile();
  }

  /**
   * Add tile to deck.
   * @param tile Tile to be added to the deck
   */
  public void addTileToDeck(Tile tile) {
    deck.addTile(tile);
  }

  /**
   * Remove a tile from the inventory of a player.
   * @param tile Tile to be removed
   * @param player Player object of which the tile must be removed
   */
  public void removeTileFromPlayer(Tile tile, Player player) {
    player.removeTile(tile);
  }

  /**
   * Add a tile to the inventory of a player.
   * @param tile Tile to be added
   * @param player Player object to which the tile must be added
   */
  public void addTileToPlayer(Tile tile, Player player) {
    player.addTile(tile);
  }

  // -------- SHOW FUNCTIONS ---------

  /**
   * Show scores of all users in the viewcontroller.
   */
  public void showScores() {
    view.showScores(getScores());
  }

  /**
   * Send message to the viewcontroller to be shown to the user.
   * @param msg String to be shown to the user
   */
  public void showMsg(String msg) {
    view.showMsg(msg);
  }

  /**
   * Show the Help section in the viewcontroller.
   */
  public void showHelp() {
    view.showHelp();
  }

  // -------- CONTROLLER FUNCTIONS ---------

  /**
   * While the game is not over, keep giving turns to players and ending the
   * turns after that. At the end of the game, show and clear the scores.
   */
  public void play() {
    while (!gameOver()) {
      getCurrentPlayer().determineMove(board);
      endOfTurn();
    }
    showMsg("Spel is afgelopen! Leaderboard:");
    view.showScores(getScores());
    
    //Clear scores of all players.
    for (int i = 0; i < amountOfPlayers; i++) {
      player.get(i).clearScore();
    }
  }

  /**
   * Finish the turn of the current player. Add the score, refill the
   * inventory, and show the next player with its board and inventory
   */
  public void endOfTurn() {
    // Finish current player
    Player current = getCurrentPlayer();
    current.addToScore(calculateScore(current, false));
    current.clearNewTiles();
    fillInventory(current);
    currentPlayer = (currentPlayer + 1) % amountOfPlayers;

    // Setup new player
    Player newPlayer = getCurrentPlayer();

    // Show message whose turn it is, the board and inventory of a
    // non-computer player.
    view.showNextPlayer();
    if (!(newPlayer instanceof ComputerPlayer)) {
      view.showBoard(getBoard());
      view.showInventory(newPlayer.getInventory(), newPlayer.getName());
    }
  }

  /**
   * Trade set of tiles with the deck.
   * @param tilesToTrade Set of tiles to be traded
   */
  public void doTrade(HashSet<Tile> tilesToTrade) {
    Player current = getCurrentPlayer();
    for (Tile t : tilesToTrade) {
      current.removeTile(t);
      deck.addTile(t);
    }
    fillInventory(current);
  }

  /**
   * Return gameover status of this game.
   * @return boolean whether the game is over or not
   */
  public boolean gameOver() {
    boolean gameover = false;
    boolean emptyInventory = false;
    for (int i = 0; i < amountOfPlayers && !emptyInventory; i++) {
      if (player.get(i).getInventory().size() == 0) {
        emptyInventory = true;
      }
    }
    if (deck.isEmpty() && emptyInventory) {
      gameover = true;
    }
    return gameover;
  }

  /**
   * Fill the inventory of a player with tiles from the deck.
   * 
   * @param player
   *      Player object of which the inventory must be filled
   */
  public void fillInventory(Player player) {
    // fill inventory of one player
    for (int i = player.getInventorySize(); i < INVENTORY_SIZE && deck.left() != 0; i++) {
      addTileToPlayer(drawTileFromDeck(), player);
    }
  }

  /**
   * Calculate and return the score of a player.
   *
   * @param player Player object of which the score will be calculated
   * @param silent Whether or not to print the amount of points gathered in this turn.
   * @return Score (integer) of Player p
   */
  public int calculateScore(Player player, boolean silent) {
    List<Tile> countedVertical = new ArrayList<Tile>();
    List<Tile> countedHorizontal = new ArrayList<Tile>();
    LinkedHashMap<Tile, String> newTiles = player.getNewTiles();
    List<String> fullRowOrColumn = new ArrayList<String>();
    int score = 0;

    // For each newly placed tile
    for (Tile t : newTiles.keySet()) {
      String coords = newTiles.get(t);
      ArrayList<Tile> tilesHorizontal = board.getHorizontalTiles(coords);
      ArrayList<Tile> tilesVertical = board.getVerticalTiles(coords);

      int horSize = tilesHorizontal.size();
      int verSize = tilesVertical.size();

      for (int i = 0; i < horSize; i++) {
        Tile hor = tilesHorizontal.get(i);
        if (!newTiles.containsKey(hor) && !countedHorizontal.contains(hor)) {
          score += 1;
          countedHorizontal.add(hor);
        }
      }

      for (int i = 0; i < verSize; i++) {
        Tile ver = tilesVertical.get(i);
        if (!newTiles.containsKey(ver) && !countedVertical.contains(ver)) {
          score += 1;
          countedVertical.add(ver);
        }
      }

      if (horSize > 0 && verSize > 0) {
        score += 2;
      } else {
        score += 1;
      }

      String xcoord = "x" + board.getX(coords);
      String ycoord = "y" + board.getY(coords);

      if (horSize == 5 && !fullRowOrColumn.contains(ycoord)) { // horizontale rij op y=y
        fullRowOrColumn.add(ycoord);
        score += 6;
      }

      if (verSize == 5 && !fullRowOrColumn.contains(xcoord)) { // verticale rij op x=x
        fullRowOrColumn.add(xcoord);
        score += 6;
      }
    }
    if (!silent) {
      showMsg("Aantal punten deze beurt: " + score);
    }
    return score;
  }

  /**
   * Start the TUI.
   */
  public void start() {
    view.start();
  }

  /**
   * Create and start a new gamecontroller for local game with TUI player
   * setup.
   * @param args Not needed
   */
  public static void main(String[] args) {
    GameController gamecontroller = new GameController(true);
    gamecontroller.start();
  }

}
