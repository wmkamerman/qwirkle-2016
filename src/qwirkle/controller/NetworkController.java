package qwirkle.controller;

import qwirkle.model.CommandPlayer;
import qwirkle.model.ComputerPlayer;
import qwirkle.model.NetworkPlayer;
import qwirkle.model.Player;
import qwirkle.model.Tile;
import qwirkle.network.Client;
import qwirkle.network.Server;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * The Class NetworkController to provide game logic for a network game.
 */
public class NetworkController extends GameController {

  /** Whether or not this NetworkController is functioning as a server or not. */
  private boolean isServer;
  
  /** The server to control. */
  private Server server;
  
  /** The client to control. */
  private Client client;

  /**
   * Setup instance of NetworkController as a server.
   * @param server The server to control
   */
  public NetworkController(Server server) {
    super(false);
    isServer = true;
    this.server = server;
  }

  /**
   * Setup instance of NetworkController as a client.
   * @param client The client to control
   */
  public NetworkController(Client client) {
    super(false);
    isServer = false;
    this.client = client;
  }
  
  /**
   * Setup instance of NetworkController for testing in main method.
   */
  public NetworkController() {
    super(false);
    isServer = false;
  }

  /**
   * Starts an instance of a game. - If server: give each commandplayer tiles
   * and send moverequest to one player. - If client: start the view and set
   * current player to the client player.
   */
  public void start() {
    if (isServer) {

      for (Player p : player) {
        List<Tile> tilesToGive = getFillInventoryTiles(p);
        String fillString = "";
        fillString += tilesToGive.size() + " ";
        for (Tile t : tilesToGive) {
          fillString += t.toNetwork() + " | ";
        }
        server.sendGiveStones(p.getName(), fillString, tilesToGive);
      }

      CommandPlayer current = (CommandPlayer) getCurrentPlayer();
      server.sendMoveRequest(current.getClientHandler());

    } else {
      view.start();
      setCurrentPlayer();
    }
  }

  /**
   * Set currentPlayer index to player with client name (so that the current
   * player of each client is the player that the client is playing).
   */
  public void setCurrentPlayer() {
    for (int i = 0; i < player.size(); i++) {
      if (client.getName().equals(player.get(i).getName())) {
        currentPlayer = i;
      }
    }
  }

  /**
   * Show the board and inventory of the current player in the view.
   */
  public void showBoardAndInventory() {
    Player current = getCurrentPlayer();
    view.showBoard(getBoard());
    if (!(current instanceof ComputerPlayer)) {
      view.showInventory(current.getInventory(), current.getName());
    }
  }

  /**
   * Get a list if tiles to fill the inventory of a player.
   * @param player Player to get fill inventory tiles from
   * @return list of tiles from deck to fill inventory of player
   */
  public List<Tile> getFillInventoryTiles(Player player) {
    List<Tile> result = new ArrayList<Tile>();
    for (int i = player.getInventorySize(); i < INVENTORY_SIZE && deck.left() != 0; i++) {
      Tile toAdd = deck.drawTile();
      result.add(toAdd);
    }
    return result;
  }

  /**
   * End the move of current player. Sends new stones to current player and sends 
   * moverequest to next player. Ends the game if the game is over.
   * Only server NetworkController executes this function when it receives a move.
   */
  public void setClientHasSetMove() {
    if (!gameOver()) {
      Player current = getCurrentPlayer();
      List<Tile> tilesToGive = getFillInventoryTiles(current);
      String fillString = "";
      fillString += tilesToGive.size() + " ";
      for (Tile t : tilesToGive) {
        fillString += t.toNetwork() + " | ";
      }
      server.sendGiveStones(current.getName(), fillString, tilesToGive);

      currentPlayer = (currentPlayer + 1) % amountOfPlayers;

      CommandPlayer newPlayer = (CommandPlayer) getCurrentPlayer();
      server.sendMoveRequest(newPlayer.getClientHandler());
    } else {
      System.out.println("Game Over!");
      view.showScores(getScores());
      server.sendGameOver(((CommandPlayer) getCurrentPlayer()).getClientHandler());
    }
  }

  /**
   * Function to end a turn: collects placed tiles and sends them to server.
   * Overrides the endOfTurn in GameController.
   * Only for a client networkcontroller, not a server
   */
  public void endOfTurn() {
    // Finish current player
    Player current = getCurrentPlayer();
    current.addToScore(calculateScore(current, false));

    String moves = current.getNewTiles().size() + " ";

    Iterator<Map.Entry<Tile, String>> it = current.getNewTiles().entrySet().iterator();
    while (it.hasNext()) {
      Map.Entry<Tile, String> pair = it.next();
      moves += pair.getKey().type.nr + " " + pair.getKey().color.nr + " " + pair.getValue();
      if (it.hasNext()) {
        // not last iteration, so add the " | " at the end
        moves += " | ";
      }
    }

    current.clearNewTiles();

    client.sendSetMove(moves);
  }

  /**
   * Place a tile on the board by player name.
   * @param coords Coordinates of the tile to be placed ("x y")
   * @param ti Tile to be placed
   * @param name Name of player
   */
  public void setClientMove(String coords, Tile ti, String name) {
    NetworkPlayer pl = getPlayerByName(name);
    board.setMove(coords, ti, true);
    pl.addToNewTiles(ti, coords);
  }
  
  /**
   * Trade set of tiles with the deck.
   * @param tilesToTrade Set of tiles to be traded
   */
  @Override
  public void doTrade(HashSet<Tile> tilesToTrade) {
    Player current = getCurrentPlayer();
    String stringToSend = tilesToTrade.size() + " ";
    for (Tile t : tilesToTrade) {
      current.removeTile(t);
      stringToSend += t.type.nr + " " + t.color.nr + " | ";
    }
    
    client.sendTrade(stringToSend);
  }

  /**
   * Set score of client player by name.
   * @param name Name of player
   */
  public void setClientScore(String name) {
    NetworkPlayer player = getPlayerByName(name);
    player.addToScore(calculateScore(player, false));
    player.clearNewTiles();
  }

  /**
   * Determine a move.
   * @param playerName the name of the player
   */
  public void determineMove(String playerName) {
    NetworkPlayer player = getPlayerByName(playerName);
    player.determineMove(board);
  }

  /**
   * Add a tile to a player by player name.
   * Used by networkcontroller to fill inventory
   * @param tile Tile to be given
   * @param playerName Name of player
   */
  public void addTileToPlayer(Tile tile, String playerName) {
    NetworkPlayer player = getPlayerByName(playerName);
    player.addTile(tile);
  }

  /**
   * Get a player object by name of that player.
   * @param name Name of player
   * @return Player object if player found, null if no player exists with that name
   */
  public NetworkPlayer getPlayerByName(String name) {
    NetworkPlayer result = null;
    for (Player p : player) {
      if (p.getName().equals(name) && p instanceof NetworkPlayer) {
        result = (NetworkPlayer) p;
      }
    }
    return result;
  }

  /**
   * Adds a player to this game.
   * @param newPlayer the player to be added
   */
  @Override
  public void addPlayer(Player newPlayer) {
    if ((amountOfPlayers + 1) < MAX_PLAYERS) {
      amountOfPlayers++;
      player.add(newPlayer);
    }
  }
  
  /**
   * Main test function.
   * @param args Not needed
   */
  public static void main(String[] args) {
    NetworkController controller = new NetworkController();
    
    Player p1 = new NetworkPlayer("Wim", controller, false, 2);
    Player p2 = new NetworkPlayer("Piet", controller, false, 2);
    
    controller.addPlayer(p1);
    controller.addPlayer(p2);
    
    // Test the getPlayerByName function
    System.out.println("Player instance of Wim is correct: " 
        + p1.equals(controller.getPlayerByName("Wim")));
    System.out.println("Player 'Klaas' exists: " + !(controller.getPlayerByName("Klaas") == null));
    
    // Test the addTileToPlayer function
    Tile t1 = controller.drawTileFromDeck();
    controller.addTileToPlayer(t1, "Wim");
    System.out.println("addTileToPlayer works as expected: " + (p1.getInventory().contains(t1)));
    
    // Test setClientScore (should be 0)
    controller.setClientScore("Wim");
    System.out.println("setClientScore working good: " + (controller.getScores().get("Wim") == 0));
    
    // Test getFillInventoryTiles
    System.out.println("Inventory size of Wim: " + p1.getInventorySize());
    System.out.println("Amount of Tiles needed to fill is correct: " 
        + (controller.getFillInventoryTiles(p1).size() == (GameController.INVENTORY_SIZE 
            - p1.getInventorySize())));
  }

}
