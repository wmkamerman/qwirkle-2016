package qwirkle.controller;

import qwirkle.model.Tile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observer;

/**
 * The Interface ViewController to provide a base for the TUI.
 */
public interface ViewController extends Observer {
  
  /** Start the TUI. */
  public void start();
  
  /**
   * Show a board in the TUI.
   * @param board the board to be shown
   */
  public void showBoard(String board);
  
  /**
   * Show next player.
   */
  public void showNextPlayer();
  
  /**
   * Show inventory of a player.
   * @param inventory the inventory as a list of tiles.
   * @param name the name of the player.
   */
  public void showInventory(ArrayList<Tile> inventory, String name);
  
  /**
   * Show error in the TUI.
   * @param error the error to be shown.
   */
  public void showError(String error);
  
  /**
   * Show a regular message in the TUI.
   * @param msg the message
   */
  public void showMsg(String msg);
  
  /**
   * Show help in the TUI.
   */
  public void showHelp();
  
  /**
   * Show scores of all players.
   * @param scores the scores as a HashMap: Playername, Score
   */
  public void showScores(HashMap<String, Integer> scores);
}
